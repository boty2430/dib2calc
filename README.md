Dib2Calc (aka Dibdib Calculator)
========

The crazy calculator. Once you get used to it, you will love it :-)

Your data is protected (encrypted) if you use your own access code and
password. In case you have a lot of data, make sure to save it to the device's
Download area via the SAVTO function.

The app requires some extra processing in the background (e.g. for loading and
saving). Depending on your system settings, this might cause a few 'hickups'
for the time being ...

If it gets 'stuck' due to some cut-down:

- First go to the device's settings, clear the app data, and try again.
- If it still cannot load your data, then open a file manager, rename the
'Download/dibdib' folder (e.g. to 'Download/dd_OLD'), and start the app
a-fresh.

An installable APK file can be downloaded from:

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.gitlab.dibdib.dib2calc/)

https://f-droid.org/packages/com.gitlab.dibdib.dib2calc
(for stable versions)

https://www.magentacloud.de/share/x5x-ox77lf
(for experimental (!) versions)

(Cmp. https://gxworks.github.io/dibdib)

Copyright (C) 2016,2017,2018,2019,2020  Roland Horsch <gx work
s{at}ma il.de >.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version (or, but only for the 'net.sf.dibdib'
    parts of the source code, the matching LGPL variant of GNU).

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License and the LICENSE file for more details.

See LICENSE file (= GPLv3-or-later)
and further details under 'LICENSE_all' or 'assets' or 'resources'
(e.g. https://github.com/gxworks/dibdib/blob/master/docs/LICENSE_all)

(Impressum: https://gxworks.github.io/impressum)

