package com.gitlab.dibdib.android_wk;

import android.app.Activity;
import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;
import com.gitlab.dibdib.android_ui.Dib2Activity;
import java.io.*;
import net.sf.dibdib.config.Dib2Constants;
import net.sf.dibdib.generic.PlatformFuncIf;
import net.sf.dibdib.thread_any.*;

/** For worker thread, but this part can also be used on other threads.
 */
public class PlatformUtil implements PlatformFuncIf {

//=====

private String[] getLicense_lines;

@Override
public String[] getLicense( String pre ) {
	if (null == getLicense_lines) {
		String license = (null == pre) ? "" : (pre + '\n');
		license += "(Version " + Dib2Constants.VERSION_STRING + ")\n";
		Activity ax = Dib2Activity.getActivity();
		if (null == ax) {
			license += "(Cannot open license files)";
		} else {
			AssetManager am = ax.getAssets();
			InputStream is;
			try {
				is = am.open( "license.txt" );
				license += MiscFunc.readStream( is );
				is = am.open( "spongycastle_license.txt" );
				license += MiscFunc.readStream( is );
				is = am.open( "javamail_license.txt" );
				license += MiscFunc.readStream( is );
				is = am.open( "apache_license2.txt" );
				license += MiscFunc.readStream( is );
			} catch (IOException e) {
				return Dib2Constants.NO_WARRANTY;
			}
		}
		getLicense_lines = license.split( "\n" );
	}
	return getLicense_lines;
}

@Override
public File getFilesDir( String... parameters ) {
	if (0 == parameters.length) {
		return null;
	} else if (StringFunc.toLowerCase( parameters[ 0 ] ).contains( "safe" )) {
		return Dib2Activity.getSafeDir();
	}
	File dir = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DOWNLOADS );
	if (!dir.exists()) {
		dir.mkdirs();
		if (!dir.exists()) {
			return null;
		}
	}
	if (StringFunc.toLowerCase( parameters[ 0 ] ).contains( "download" ) && !StringFunc.toLowerCase( parameters[ 0 ] ).contains( "sub" )) {
		return dir;
	}
	File sub = new File( dir, "dibdib" );
	if (!sub.exists()) {
		sub.mkdirs();
	}
	if (!sub.isDirectory()) {
		return dir;
	}
	return sub;
}

@Override
public void log( String... aMsg ) {
	Log.d( aMsg[ 0 ], aMsg[ 1 ] );
}

@Override
public void invalidate() {
	Dib2Activity.invalidate();
}

//=====

}
