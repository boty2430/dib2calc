// Copyright (C) 2020  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package com.gitlab.dibdib.android_wk;

import android.os.AsyncTask;
import net.sf.dibdib.thread_wk.CcmRunner;

/** For worker thread.
 */
public abstract class WorkerTask extends AsyncTask< String, String, String > {
//=====

@Override
protected String doInBackground( String... params ) {
	// Log.d( "back", "" + UiCalcServer.INSTANCE.qPageLinesStable.lines[ 0 ] );
	if (!isCancelled()) {
		CcmRunner.INSTANCE.run();
	}
	return null;
}

public void invalidate() {
	// Log.d( "inval", UiCalcServer.INSTANCE.qTextLines.lines[ 0 ] );
	publishProgress();
}

//=====
}
