// Copyright (C) 2016,2017,2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package com.gitlab.dibdib.common;

import net.sf.dibdib.generic.TcvFunc;

import com.gitlab.dibdib.joined.CodecAlgoFunc;

import java.security.*;
import java.security.spec.*;
import java.util.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.TsvCodecIf;
import net.sf.dibdib.thread_any.*;

public class TcvCodecAes implements TsvCodecIf {
//=====

public static final TcvCodecAes instance = new TcvCodecAes();

private static short zPbkdVariantTempAsWorkaround = 0;

@Override
public byte[] init( char platform, Object... parameters ) {
	if ((1 < parameters.length) && (parameters[ 0 ] instanceof String)
		&& (parameters[ 1 ] instanceof Short)
		&& "VAR".equals( parameters[ 0 ] )) {
		// For workarounds ...
		zPbkdVariantTempAsWorkaround = (Short) parameters[ 1 ];
		return null;
	}
	CodecAlgoFunc.INIT_cryptoAes_OLD = (Dib2Config.platformMarker == '0') ? "AES/CBC/PKCS5Padding" :
		"AES/CBC/PKCS7Padding";
	return null;
}

@Override
public byte[] compress( byte[] xyData, int xOffset4Reuse, int to ) {
	return MiscFunc.compress( 'z', xyData, xOffset4Reuse, to );
}

@Override
public byte[] decompress( byte[] xData, int len ) {
	return MiscFunc.decompress( xData, 0, len );
}

@Override
public String getKeyInfo( HashSet< String > entries ) {
	StringBuilder out = new StringBuilder( entries.size() * 30 );
	byte[] pk = CcmSto.instance.preference_get( "KEY." + "0" + ".SIG.ECDSA256.P" );
	if (null != pk) {
		out.append( "Fingerprint " + CodecAlgoFunc.fingerprint( pk, false ) );
		out.append( '\n' );
	}
	for (String entry : entries) {
		int i0 = entry.indexOf( ":EMAIL:" );
		if (0 < i0) {
			String email = entry.substring( i0 + 7 );
			i0 = email.indexOf( '\t' );
			email = ((0 >= i0) ? email : email.substring( 0, i0 )).trim();
			pk = CcmSto.instance.preference_get( "KEY." + email + ".SIG.ECDSA256.P" );
			if (null != pk) {
				out.append( email + '\t' + CodecAlgoFunc.fingerprint( pk, false ) );
				out.append( '\n' );
			}
		}
	}
	return out.toString();
}

@Override
public byte[] getKey( byte[] pass, byte[] accessCode, byte[] xSalt16, int iterations ) {
	byte[] key = null;
	if (3 > iterations) {
		return null;
	}
	try {
		if (null == accessCode) {
			// Old format.
			final char[] pass32 = new String( CodecAlgoFunc.toPass32( pass, null, true )
				) //, StringFunc.STR256 )
				.toCharArray();
			//zAesKeySpec = new SecretKeySpec( toPass32( passphrase ), "AES" );
			//PKCS5S2ParametersGenerator generator = new PKCS5S2ParametersGenerator( new SHA256Digest() );
			//generator.init( pass32b, ivSalt16, INIT_saltIterations );
			//KeyParameter key = (KeyParameter) generator.generateDerivedMacParameters( 256 );
			//zAesKeySpec = new SecretKeySpec( key.getKey(), "AES" );
			PBEKeySpec kspec = new PBEKeySpec( pass32, xSalt16, iterations, 256 );
			key = new SecretKeySpec( //.
				SecretKeyFactory.getInstance( CodecAlgoFunc.INIT_cryptoPbkd ).generateSecret( kspec ).getEncoded(),
				"AES" ).getEncoded();
			kspec.clearPassword();
		} else {
			byte[] secret;
			if (0 >= zPbkdVariantTempAsWorkaround) {
				zPbkdVariantTempAsWorkaround = 0;
				// Poor Java, having fallen into the UTF-16 trap, affecting also this side of the world ... :-)
				// We have to compensate in a somewhat silly way for the sake of portability.
				final char[] hex = StringFunc.hex4Bytes( pass, false ).toCharArray();
				PBEKeySpec kspec = new PBEKeySpec( hex, xSalt16, iterations, 128 );
				final byte[] secret0 = SecretKeyFactory.getInstance( CodecAlgoFunc.INIT_cryptoPbkd ).generateSecret( kspec ).getEncoded();
				kspec.clearPassword();
				MessageDigest md = MessageDigest.getInstance( "SHA-256" );
				md.update( secret0 );
				md.update( pass );
				secret = md.digest( accessCode );
			} else {
				byte[] pass32 = CodecAlgoFunc.toPass32( pass, accessCode, true );
				// For devices that used the former workaround ...
				char[] pass32Compensated = StringFunc.string4Ansi( pass32 ).toCharArray();
				if (2 == zPbkdVariantTempAsWorkaround) {
					// For interop with other such devices ...
					pass32 = StringFunc.bytesUtf8( new String( pass32Compensated ) );
					pass32Compensated = StringFunc.string4Ansi( pass32 ).toCharArray();
				}
				// Do not keep using the former workaround.
				zPbkdVariantTempAsWorkaround = 0;
				PBEKeySpec kspec = new PBEKeySpec( Arrays.copyOfRange( pass32Compensated, 0, 16 ), xSalt16, iterations, 128 );
				final byte[] secret0 = SecretKeyFactory.getInstance( CodecAlgoFunc.INIT_cryptoPbkd ).generateSecret( kspec ).getEncoded();
				kspec.clearPassword();
				kspec = new PBEKeySpec( Arrays.copyOfRange( pass32Compensated, 16, 32 ), xSalt16, iterations, 128 );
				final byte[] secret1 = SecretKeyFactory.getInstance( CodecAlgoFunc.INIT_cryptoPbkd ).generateSecret( kspec ).getEncoded();
				kspec.clearPassword();
				secret = Arrays.copyOf( secret0, 32 );
				System.arraycopy( secret1, 0, secret, 16, 16 );
			}
			key = new SecretKeySpec( secret, "AES" ).getEncoded();
		}
	} catch (Exception e) {
		key = null;
	}
	return key;
}

@Override
public byte[] encodePhrase( byte[] phrase, byte[] accessCode ) {
	return CodecAlgoFunc.toPass32( accessCode, phrase, true );
}

@Override
public byte[] decodePhrase( byte[] encoded, byte[] accessCode ) {
	return CodecAlgoFunc.fromPass32( accessCode, encoded );
}

private byte[] encode_OLD( byte[] compressedData, int from, int to, byte[] key, int keyInfo, byte[] keyData, byte[] signatureKey )
	throws Exception {
	// PKCS7 or 5: 'AES/CBC/PKCS7Padding'/5
	Cipher cipher = Cipher.getInstance( CodecAlgoFunc.INIT_cryptoAes_OLD );
	int preBlocks = (16 + keyData.length + 15) / 16;
	int postBlocks = 5;
	byte[] iv16 = TcvCodec
		.createHeaderSaltIv16_OLD( Dib2Constants.MAGIC_BYTES, new byte[] { (byte) preBlocks, (byte) postBlocks, (byte) keyInfo }, this );
	SecretKeySpec skspec = new SecretKeySpec( key, "AES" );
	cipher.init( Cipher.ENCRYPT_MODE, skspec, new IvParameterSpec( iv16 ) );
//	assert Arrays.equals( iv16, cipher.getParameters().getParameterSpec( IvParameterSpec.class ).getIV() );
	byte[] enc = cipher.doFinal( compressedData, from, to - from );
	PKCS8EncodedKeySpec ecSpec = new PKCS8EncodedKeySpec( signatureKey );
	Signature dsa = Signature.getInstance( "SHA256withECDSA" );
	dsa.initSign( KeyFactory.getInstance( "EC" ).generatePrivate( ecSpec ) );
	dsa.update( keyData );
	if (keyData.length < ((preBlocks - 1) * 16)) {
		dsa.update( new byte[ ((preBlocks - 1) * 16) - keyData.length ] );
	}
	dsa.update( enc );
	byte[] signature = dsa.sign();
	if (signature.length > (postBlocks * 16)) {
		return null;
	}
	int iPad = signature.length;
	signature = Arrays.copyOf( signature, postBlocks * 16 );
	signature[ iPad ] = (byte) 0x80;
	byte[] out = new byte[ 8 + preBlocks * 16 + enc.length + postBlocks * 16 ];
	System.arraycopy( iv16, 0, out, 8, iv16.length );
	System.arraycopy( keyData, 0, out, 8 + iv16.length, keyData.length );
	System.arraycopy( enc, 0, out, 8 + preBlocks * 16, enc.length );
	System.arraycopy( signature, 0, out, 8 + preBlocks * 16 + enc.length, signature.length );
	out = MiscFunc.packet4880X( Dib2Constants.RFC4880_EXP2, null, out, 8, out.length );
	return Arrays.copyOfRange( out, out[ 0 ], out.length );
}

@Override
public byte[] encode( byte[] compressedData, int from, int to, byte[] key, byte[] iv16, int oldKeyInfo, byte[] header, byte[] signatureKey )
	throws Exception {
	if (null == header) { // signatureKey) {
		// Old file format.
		byte[] out = CodecAlgoFunc.encrypt_CBC( compressedData, from, to, key,
			// MiscFunc.createHeaderSalt16( Dib2Constants.MAGIC_BYTES,
			// new byte[] { 1, 0, 0 }, this ),
			iv16,
			8 );
		int end = out.length - 8;
		out = MiscFunc.packet4880X( Dib2Constants.RFC4880_EXP2, null, out, 8, end );
		return Arrays.copyOfRange( out, out[ 0 ], end );
	}
	if (0 < oldKeyInfo) {
		// Old chat format.
		return encode_OLD( compressedData, from, to, key, oldKeyInfo, header, signatureKey );
	}
	Cipher cipher = Cipher.getInstance( "AES/CTR/NoPadding" );
	if (null == iv16) {
		iv16 = getInitialValue( 16 ); //Codec.createHeaderSalt16( Dib2Constants.MAGIC_BYTES, null, this );
	}
	SecretKeySpec skspec = new SecretKeySpec( key, "AES" );
	byte[] iv = iv16;
	if (null != signatureKey) {
		PKCS8EncodedKeySpec ecSpec = new PKCS8EncodedKeySpec( signatureKey );
		Signature dsa = Signature.getInstance( "SHA256withECDSA" );
		dsa.initSign( KeyFactory.getInstance( "EC" ).generatePrivate( ecSpec ) );
		// Last tag is skipped => join arrays:
		dsa.update( header, 0, header.length - 1 );
		dsa.update( iv16 );
		dsa.update( compressedData, from, to - from );
		// As SIV:
		iv = dsa.sign();
	}
	cipher.init( Cipher.ENCRYPT_MODE, skspec, new IvParameterSpec( iv, iv.length - 16, 16 ) );
	byte[] enc = cipher.doFinal( compressedData, from, to - from );
	if (null == signatureKey) {
		iv16 = iv;
		iv = new byte[ 0 ];
	}
	int offs = 8;
	// Join arrays => last tag is skipped
	byte[] out = new byte[ offs + 1 + header.length - 1 + iv16.length + 1 + enc.length + 5 + iv.length + 2 ];
	System.arraycopy( header, 0, out, offs, header.length - 1 );
	System.arraycopy( iv16, 0, out, offs = offs + header.length - 1, iv16.length );
	out[ offs = offs + iv16.length ] = (byte) (TcvFunc.TAG_BYTES_L0 + iv16.length);
	int len = enc.length;
	System.arraycopy( enc, 0, out, ++ offs, len );
	out[ offs = offs + len ] = (byte) len;
	out[ ++ offs ] = (byte) (len >>> 8);
	if (len < (1 << 16)) {
		out[ ++ offs ] = TcvFunc.TAG_BYTES_L2;
	} else {
		out[ ++ offs ] = (byte) (len >>> 16);
		out[ ++ offs ] = (byte) (len >>> 24);
		out[ ++ offs ] = TcvFunc.TAG_BYTES_L4;
	}
	System.arraycopy( iv, 0, out, ++ offs, iv.length );
	out[ offs = offs + iv.length ] = (byte) iv.length;
	out[ ++ offs ] = TcvFunc.TAG_BYTES_L1;
	out[ ++ offs ] = (byte) (TcvFunc.TAG_ARR_C0 + 3 + (0xf & header[ header.length - 1 ]));
	out = MiscFunc.packet4880X( Dib2Constants.RFC4880_EXP2, null, out, 8, ++ offs );
	return Arrays.copyOfRange( out, out[ 0 ], offs );
}

private byte[] decode_OLD( byte[] data, int from, int to, byte[] key, byte[] signatureKey ) throws Exception {
	int offs = from;
	if (data[ offs ] == (byte) Dib2Constants.RFC4880_EXP2) {
		offs += MiscFunc.getPacketHeaderLen( data, offs );
	}
	int headerlen = 16;
	int version = 10 * data[ offs + 2 ];
	byte[] info = new byte[ 3 ];
	System.arraycopy( data, offs + 4, info, 0, 3 );
	final byte keyInfo = info[ 2 ];
	if ((10 < version) && (0 != keyInfo)) {
		if (30 > version) {
			// Waiting for Android support ...
			return null; // UtilCodec.decrypt256( data, offs + headerlen, to, keyOrPass, Arrays.copyOfRange( data, offs, offs + 16 ) );
		}
		Cipher cipher = Cipher.getInstance( CodecAlgoFunc.INIT_cryptoAes_OLD );
		byte[] iv = Arrays.copyOfRange( data, offs, offs + 16 );
		SecretKeySpec skspec = new SecretKeySpec( key, "AES" );
		cipher.init( Cipher.DECRYPT_MODE, skspec, new IvParameterSpec( iv ) );
		headerlen = 16 * data[ offs + 2 + Dib2Constants.MAGIC_BYTES.length ];
		int trailerlen = 16 * data[ offs + 3 + Dib2Constants.MAGIC_BYTES.length ];
		if (0 >= trailerlen) {
			Dib2Config.log( "aes decode", "Missing signature." );
			return null;
		}
		byte[] signature = Arrays.copyOfRange( data, to - trailerlen, to );
		for (int i0 = signature.length - 1; i0 > 40; -- i0) {
			if (signature[ i0 ] == (byte) 0x80) {
				signature = Arrays.copyOf( signature, i0 );
				break;
			}
		}
		X509EncodedKeySpec ecSpec = new X509EncodedKeySpec( signatureKey );
		Signature dsa = Signature.getInstance( "SHA256withECDSA" );
		dsa.initVerify( KeyFactory.getInstance( "EC" ).generatePublic( ecSpec ) );
		dsa.update( data, offs + 16, to - trailerlen - offs - 16 );
		if (!dsa.verify( signature )) {
			Dib2Config.log( "aes decode", "Unexpected signature." );
			return null;
		}
		byte[] out = cipher.doFinal( data, offs + headerlen, to - trailerlen - offs - headerlen );
		return out;
	}
	return CodecAlgoFunc.decrypt_CBC( data, offs + headerlen, to, key, Arrays.copyOfRange( data, offs, offs + 16 ) );
}

@Override
public byte[] decode( byte[] data, int from, int to, byte[] key, byte[] signatureKey ) throws Exception {
	int offs = from;
	if (data[ offs ] == (byte) Dib2Constants.RFC4880_EXP2) {
		final int hdlen = MiscFunc.getPacketHeaderLen( data, offs );
		to = offs + hdlen + MiscFunc.getPacketBodyLen( data, 1 + offs );
		offs += hdlen;
		if ((to > data.length) || ((to - 16) <= from)) {
			return null;
		}
	}
	if (data[ offs ] != Dib2Constants.MAGIC_BYTES[ 0 ]) {
		return null;
	}
	int headerlen = 16;
	if (0 == data[ offs + 6 ]) {
		// Old file format.
		return CodecAlgoFunc.decrypt_CBC( data, offs + headerlen, to, key, Arrays.copyOfRange( data, offs, offs + 16 ) );
	}
	// Check version and key info (0 = local).
	int version = data[ offs + 2 ];
	version = (6 >= version) ? 50 : data[ offs + 4 ];
	if (60 > version) {
		return decode_OLD( data, from, to, key, signatureKey );
	} else if (Dib2Constants.FILE_STRUC_VERSION < version) {
		// Future version, not backwards compatible ...
		return null;
	}
	// SIG/SIV
	long offsLen = TcvFunc.getTcvOffsetLength( data, offs, data.length - 2, 1 );
	final int iSig = (int) offsLen;
	final int cSig = (int) (offsLen >>> 32);
	Cipher cipher = Cipher.getInstance( "AES/CTR/NoPadding" );
	SecretKeySpec skspec = new SecretKeySpec( key, "AES" );
	// ENC
	offsLen = TcvFunc.getTcvOffsetLength( data, offs, -1 + (int) offsLen, 1 );
	if (3 > (int) (offsLen >>> 32)) {
		return null;
	}
	final long jEnc = offsLen;
	// IV
	offsLen = TcvFunc.getTcvOffsetLength( data, offs, -1 + (int) offsLen, 1 );
	cipher.init( Cipher.DECRYPT_MODE, skspec,
		(0 < cSig) ? new IvParameterSpec( data, iSig + cSig - 16, 16 )
			: new IvParameterSpec( data, (int) offsLen, 16 ) );
	byte[] compressed = cipher.doFinal( data, (int) jEnc, (int) (jEnc >>> 32) );
	if (16 != (int) (offsLen >>> 32)) {
		return null;
	}
	if (null != signatureKey) {
		if (16 >= cSig) {
			return null;
		}
		X509EncodedKeySpec ecSpec = new X509EncodedKeySpec( signatureKey );
		Signature dsa = Signature.getInstance( "SHA256withECDSA" );
		dsa.initVerify( KeyFactory.getInstance( "EC" ).generatePublic( ecSpec ) );
		dsa.update( data, offs, (int) offsLen - offs );
		dsa.update( data, (int) offsLen, 16 );
		dsa.update( compressed );
		if (!dsa.verify( data, iSig, cSig )) {
			return null;
		}
	}
	return compressed;
}

@Override
public byte getMethodTag() {
	return 'A'; // AES
}

@Override
public byte[] getInitialValue( int len ) {
	byte[] rand = new byte[ len ];
	new SecureRandom().nextBytes( rand );
	long uid = MiscFunc.getUidBase();
	if (10 < len) {
		rand[ 0 ] = (byte) (uid >>> 48);
		rand[ 1 ] = (byte) (uid >>> 40);
		rand[ 2 ] = (byte) (uid >>> 32);
		if (0 != (short) uid) {
			rand[ 3 ] = (byte) (uid >>> 8);
			rand[ 4 ] = (byte) uid;
		}
	}
	return rand;
}

//=====
}
