// Copyright (C) 2020  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package com.gitlab.dibdib.dib2calc;

import net.sf.dibdib.thread_ui.UiDataSto;

import android.os.Bundle;
import android.util.Log;
import com.gitlab.dibdib.android_ui.Dib2Activity;
import net.sf.dibdib.thread_ui.CalcPres;

public final class Main extends Dib2Activity {
//=====

@Override
protected void onCreate( Bundle savedInstanceState ) {
	super.onCreate( savedInstanceState );
	init( "calc", CalcPres.INSTANCE );
	Log.d( "onCreate", "." + UiDataSto.qSwitches[ UiDataSto.SWI_STEP_OR_ACCESS_MINUTE ] + '.' + UiDataSto.pathInitDataFile );
}

//=====
}
