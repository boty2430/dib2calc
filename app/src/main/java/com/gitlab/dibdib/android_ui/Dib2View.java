// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package com.gitlab.dibdib.android_ui;

import android.content.Context;
import android.graphics.*;
import android.util.*;
import android.view.*;
import java.util.Arrays;
import net.sf.dibdib.config.Dib2Local;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_ui.*;
import net.sf.dibdib.thread_wk.CcmRunner;

import static net.sf.dibdib.thread_ui.UiDataSto.*;

public class Dib2View extends View {
//=====

volatile boolean qResuming = false;

private final Paint mToolPaint;
private final Paint mToolPaint2;
private final Paint mTextPaint;
private final Paint mLinePaint;
private final Rect zBounds = new Rect();
private float zX;
private float zY;
private float zLastX;
private float zLastY;
private long zLastTime;
private long zTouchTime;

public Dib2View( Context context ) {
	super( context );
	setFocusable( true );
	setFocusableInTouchMode( true );
	mTextPaint = new Paint( Paint.ANTI_ALIAS_FLAG );
	mTextPaint.setTextAlign( Paint.Align.LEFT );
	mToolPaint = new Paint( Paint.ANTI_ALIAS_FLAG );
	mToolPaint.setTextSize( WIN_FONT_SIZE_ANDROID >> UiDataSto.qShiftView );
	mToolPaint.setTextAlign( Paint.Align.LEFT );
	mToolPaint2 = new Paint( Paint.ANTI_ALIAS_FLAG );
	mToolPaint2.setTextSize( (3 * WIN_FONT_SIZE_ANDROID) >> (2 + UiDataSto.qShiftView) );
	mToolPaint2.setTextAlign( Paint.Align.LEFT );
	mLinePaint = new Paint( Paint.ANTI_ALIAS_FLAG );
	//mLinePaint.setColor( 0 );
	mLinePaint.setStyle( Paint.Style.STROKE );
}

private void touch_start( float x, float y ) {
	zX = x;
	zY = y;
	zTouchTime = MiscFunc.currentTimeMillisLinearized();
}

private void touch_move( float x, float y ) {
	float ex = Math.abs( x - zX );
	float ey = Math.abs( y - zY );
	final float tolerance = TOUCH_TOLERANCE >>> UiDataSto.qShiftView;
	if (ex > tolerance || ey > tolerance) {
		zX = -1;
		zY = -1;
	}
}

private void touch_end() {
	long time = MiscFunc.currentTimeMillisLinearized();
	if ((0 > zX) || (0 > zY)) {
		zLastTime = 0;
		zLastX = -1;
		return;
	}
	long bClickclickLong = 0;
	if ((0 < zTouchTime) && ((zTouchTime + 1500) <= time)) {
		bClickclickLong |= 0x2;
	}
	if ((0 <= zLastX) && (0 < zLastTime) && ((zLastTime + 4500) >= time)) {
		float ex = Math.abs( zLastX - zX );
		float ey = Math.abs( zLastY - zY );
		final float tolerance = TOUCH_TOLERANCE >>> UiDataSto.qShiftView;
		if ((ex <= tolerance) && (ey <= tolerance)) {
			bClickclickLong |= 0x1;
		}
	}
	zLastX = zX;
	zLastY = zY;
	zLastTime = time;
	char key = CalcPres.INSTANCE.touch( (int) zX, (int) zY, UiDataSto.qWinOffsetXPixR,
		(UiDataSto.qPYLineOffs * WIN_LINE_SPACING_ANDROID) >>> UiDataSto.qShiftScale, bClickclickLong );
	Log.d( "touch", "" + (int) key );
	switch (key) {
		case 0:
			break;
		case StringFunc.ESCAPE:
			break;
		default:
			Log.d( "touch", "2x" + (int) key );
			if (null != Dib2Activity.qActivity) {
				Dib2Activity.qActivity.triggerWorker( key, (StringFunc.DONE == key) );
			}
	}
}

@Override
public boolean onTouchEvent( MotionEvent event ) {
	float x = event.getX();
	float y = event.getY();

	switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			touch_start( x, y );
			break;
		case MotionEvent.ACTION_MOVE:
			touch_move( x, y );
			break;
		case MotionEvent.ACTION_UP:
			touch_end();
//			if (//CalcRpr.INSTANCE.qDoDraw || 
//			(CalcRpr.INSTANCE.wqRefreshNeeded)) {
			invalidate();
//			}
			break;
	}
	return true;
}

@Override
protected void onMeasure( int widthMeasureSpec, int heightMeasureSpec ) {
	UiDataSto.qWinXPixR = MeasureSpec.getSize( widthMeasureSpec );
	UiDataSto.qWinYPixR = MeasureSpec.getSize( heightMeasureSpec );
	setMeasuredDimension( UiDataSto.qWinXPixR, UiDataSto.qWinYPixR );
	if (null == Dib2Activity.qActivity) {
		return;
	}
	DisplayMetrics dm = Dib2Activity.qActivity.getApplicationContext().getResources().getDisplayMetrics();
	// Logical size, based on default dpi values ...
	float xLog = dm.widthPixels / dm.density;
	float yLog = dm.heightPixels / dm.density;
	UiDataSto.qWinWidthPixL = (int) (xLog * PIXL_DPI);
	UiDataSto.qWinHeightPixL = (int) (yLog * PIXL_DPI);
	UiDataSto.qPixRDpiX = (int) dm.xdpi;
	UiDataSto.qPixRDpiY = (int) dm.ydpi;
	UiDataSto.qShiftView = PIXR_SHIFT_DEFAULT;
	if ((200 <= UiDataSto.qPixRDpiX) && (600 <= UiDataSto.qWinXPixR)) {
		int winX = UiDataSto.qWinXPixR;
		int dpi = UiDataSto.qPixRDpiX;
		// 160 dpi and 320/480 pixel as old standard:
		for (; (dpi >= 180) && (winX >= 320 * dpi / 160); dpi >>= 1, winX >>= 1) {
			-- UiDataSto.qShiftView;
		}
		UiDataSto.qShiftView = (0 >= UiDataSto.qShiftView) ? 0 : UiDataSto.qShiftView;
	}
	UiDataSto.qShiftScale = UiDataSto.qShiftView;
	mTextPaint.setTextSize( 1 + (WIN_FONT_SIZE_ANDROID >> UiDataSto.qShiftScale) );
	mToolPaint.setTextSize( WIN_FONT_SIZE_ANDROID >> UiDataSto.qShiftView );
	mToolPaint2.setTextSize( (3 * WIN_FONT_SIZE_ANDROID) >> (2 + UiDataSto.qShiftView) );
	UiDataSto.qWinY1TopBarsPixR = WIN_TOP_PIXL >>> UiDataSto.qShiftView;
	UiDataSto.qWinEYDialogBarPixR = WIN_BOTTOM_PIXL >>> UiDataSto.qShiftView;
}

private void drawTextLines( CcmTextLines xTextLines, int xAlpha, int xRed, Canvas canvas ) {
	mTextPaint.setTextSize( 1 + (WIN_FONT_SIZE_ANDROID >> UiDataSto.qShiftScale) );
	mTextPaint.setARGB( xAlpha, xRed, 0, 0 );
	int nX = -UiDataSto.qWinOffsetXPixR;
	int nY = ((3 * WIN_LINE_SPACING_ANDROID / 2) >>> UiDataSto.qShiftScale) + UiDataSto.qWinY1TopBarsPixR;
	final int eY = WIN_LINE_SPACING_ANDROID >>> UiDataSto.qShiftScale;
	for (int i0 = UiDataSto.qPYLineOffs; (i0 < xTextLines.count)
		&& (nY < (UiDataSto.qWinYPixR - UiDataSto.qWinEYDialogBarPixR)); i0 ++) {
		final String txt = (null == xTextLines.lines[ i0 ]) ? "" : xTextLines.lines[ i0 ];
		int ix1 = txt.indexOf( '\t' );
		if (0 <= ix1) {
			final int len = txt.length();
			int ix0 = 0;
			mTextPaint.getTextBounds( "_", 0, 1, zBounds );
			final int spaceWidth = zBounds.right + 1;
			int nxp = nX;
			for (; ix0 < len; ix1 = txt.indexOf( '\t', ix0 )) {
				ix1 = (0 > ix1) ? txt.length() : ix1;
				if (ix0 < ix1) {
					canvas.drawText( txt, ix0, ix1, nxp, nY, mTextPaint );
					mTextPaint.getTextBounds( txt, ix0, ix1, zBounds );
					nxp += zBounds.right;
				}
				nxp = (1 + (nxp / spaceWidth + 2) / 8) * spaceWidth * 8;
				ix0 = ix1 + 1;
			}
		} else if (0 < txt.length()) {
			canvas.drawText( txt, nX, nY, mTextPaint );
		}
		nY += eY;
	}
}

private void drawKeyPad( Canvas canvas, int xAlpha, int xGreen ) {

	final int eY = 1
		+ (UiDataSto.qWinYPixR - UiDataSto.qWinY1TopBarsPixR - UiDataSto.qWinEYDialogBarPixR)
		/ UiDataSto.qcKeys4Win;
	int nY = UiDataSto.qWinY1TopBarsPixR + eY / 2;
	final int eX = 1 + UiDataSto.qWinXPixR / UiDataSto.qcKeys4Win;
	mToolPaint.setARGB( xAlpha, 0, xGreen, 0 );
	if (900 <= UiDataSto.qSwitches[ UiDataSto.SWI_STEP_OR_ACCESS_MINUTE ]) {
		mToolPaint.setFakeBoldText( true );
	}
//	if (0 != ((SW_FLAG_KEYPAD_Opaque | SW_FLAG_KEYPAD_Active) & UiSwitches.qSwitches[ SWI_LAYERS ])) {
	final int nY0 = nY;
	for (int i1 = 0; i1 < UiDataSto.qcKeys4Win; ++ i1) {
		int nX = UiDataSto.WIN_X_MARGIN;
//			nX = eX / 2;
		for (int i0 = 0; i0 < UiDataSto.qcKeys4Win; ++ i0) {
			char tK = UiDataSto.qPadKeys[ UiDataSto.qSwitches[ SWI_KEYPAD_INDEX ] ][ i1 * UiDataSto.qcKeys4Win
				+ i0 ];
			if (' ' <= tK) {
				canvas.drawText( "" + tK, nX, nY, mToolPaint );
			}
			nX += eX;
		}
		nY += eY;
	}
	mToolPaint.setARGB( xAlpha, 0, xGreen, xGreen );
	nY = nY0;
	for (int i1 = 0; i1 < UiDataSto.qcKeys4Win; ++ i1) {
		int nX = UiDataSto.WIN_X_MARGIN;
//			nX = eX / 2;
		for (int i0 = 0; i0 < UiDataSto.qcKeys4Win; ++ i0) {
			char tK = UiDataSto.qPadKeys[ UiDataSto.qSwitches[ SWI_KEYPAD_INDEX ] ][ i1 * UiDataSto.qcKeys4Win
				+ i0 ];
			if (' ' > tK) {
				canvas.drawText( "" + kControlAsButton[ tK ], nX, nY, mToolPaint );
			}
			nX += eX;
		}
		nY += eY;
	}
	mToolPaint.setFakeBoldText( false );
}

private void drawMarkers( Canvas canvas ) {

	///// Borders
	mLinePaint.setARGB( 0xff, 0, 0, 0 );
	int nY = UiDataSto.qWinY1TopBarsPixR / 3;
	canvas.drawLine( 0, nY, UiDataSto.qWinXPixR, nY, mLinePaint );
	nY += nY + 1;
	canvas.drawLine( 0, nY, UiDataSto.qWinXPixR, nY, mLinePaint );
	canvas.drawLine( UiDataSto.qWinXPixR / 2, nY, UiDataSto.qWinXPixR / 2,
		UiDataSto.qWinY1TopBarsPixR,
		mLinePaint );
	canvas.drawLine( 0, UiDataSto.qWinY1TopBarsPixR, UiDataSto.qWinXPixR,
		UiDataSto.qWinY1TopBarsPixR,
		mLinePaint );
	canvas.drawLine( -UiDataSto.WIN_X_MARGIN - UiDataSto.qWinOffsetXPixR, UiDataSto.qWinY1TopBarsPixR,
		-UiDataSto.WIN_X_MARGIN
			- UiDataSto.qWinOffsetXPixR, UiDataSto.qWinYPixR
			- UiDataSto.qWinEYDialogBarPixR, mLinePaint );
	canvas.drawLine( 0, UiDataSto.qWinYPixR - UiDataSto.qWinEYDialogBarPixR, UiDataSto.qWinXPixR,
		UiDataSto.qWinYPixR - UiDataSto.qWinEYDialogBarPixR, mLinePaint );
	final int oY = (UiDataSto.qPYLineOffs >>> UiDataSto.qShiftView) - UiDataSto.qWinY1TopBarsPixR;

	///// Cursors
	for (int i0 = 2; i0 >= 0; -- i0) {
		if (0 >= UiDataSto.qWinTouchedY3PixR[ i0 ]) {
			continue;
		}
		final int nX = UiDataSto.qWinTouchedX3PixR[ i0 ] - UiDataSto.qWinOffsetXPixR;
		nY = UiDataSto.qWinTouchedY3PixR[ i0 ] - oY;
		mLinePaint.setARGB( 0xff, (i0 == 0) ? 0xff : 0, (i0 == 2) ? 0xaa : 0, (i0 == 1) ? 0xee : 0 );
		canvas.drawLine( nX - 3, nY, nX + 3, nY, mLinePaint );
		canvas.drawLine( nX, nY - 3, nX, nY + 3, mLinePaint );
	}
}

private void drawDialogTextField( Canvas canvas, int nY ) {

	mToolPaint.setARGB( 0xff, 0, 0, 0 );
//	final int nYBottom = (CalcRpr.INSTANCE.qWinYPixR - 4 - (WIN_LINE_SPACING_ANDROID >>> (2 + CalcRpr.INSTANCE.qShiftView)));
	int nX = UiDataSto.qWinXPixR / 2; // CalcRpr.INSTANCE.WIN_X_MARGIN;
//	int nY = nYBottom;
	int maxLen = UiDataSto.qBarDialog[ BARI_TEXTFIELD ].length() - UiDataSto.qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ];
	maxLen = (20 < maxLen) ? 20 : maxLen;
	canvas.drawText(
		UiDataSto.qBarDialog[ BARI_TEXTFIELD ].substring( UiDataSto.qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ],
			UiDataSto.qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ]
				+ maxLen ),
		nX + 2, nY, mToolPaint );
	nX = nX - 2; //CalcRpr.INSTANCE.qWinXPixR - CalcRpr.INSTANCE.WIN_X_MARGIN;
//	nY -= CalcRpr.INSTANCE.qWinEYDialogBarPixR / 2;
	mToolPaint.setTextAlign( Paint.Align.RIGHT );
	maxLen = UiDataSto.qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ];
	maxLen = (80 <= maxLen) ? 80 : maxLen;
	canvas.drawText(
		UiDataSto.qBarDialog[ BARI_TEXTFIELD ].substring( UiDataSto.qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] - maxLen,
			UiDataSto.qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] ), nX, nY,
		mToolPaint );
	mToolPaint.setTextAlign( Paint.Align.LEFT );
//	return nYBottom;
}

private void drawToolBars( Canvas canvas ) {
	final int eX = 1 + UiDataSto.qWinXPixR / 3 / BAR_MAX_PER_SIDE;
	final int nYBottom = (UiDataSto.qWinYPixR - 4 - (WIN_LINE_SPACING_ANDROID >>> (2 + UiDataSto.qShiftView)));

	///// Dialog text field
//	final int nYBottom =drawDialogTextField( canvas );

	///// Other bar contents
	String[][] bars = new String[][] { Arrays.copyOfRange( UiDataSto.qBarTitle, 0, 2 * BAR_MAX_PER_SIDE ), //, 
		Arrays.copyOfRange( UiDataSto.qBarTitle, 2 * BAR_MAX_PER_SIDE, 4 * BAR_MAX_PER_SIDE ), //, 
		Arrays.copyOfRange( UiDataSto.qBarTitle, 4 * BAR_MAX_PER_SIDE, 6 * BAR_MAX_PER_SIDE ), //, 
		UiDataSto.qBarTool,
		UiDataSto.qBarDialog,
		UiDataSto.qBarStatus };
	int nX = UiDataSto.WIN_X_MARGIN;
	final int eY = WIN_FONT_SIZE_ANDROID >> UiDataSto.qShiftView; // Dib2UiW.LPIX_PER_PT_ROUNDED >>> (2 + Dib2UiW.LPIX_SHIFT_DELTA_ANDROID);
	int nY = UiDataSto.qWinY1TopBarsPixR / 3 - 2 * eY / 3;
	for (int i1 = 0; i1 < bars.length; ++ i1) {
		int i0 = (UiDataSto.qBarDialog == bars[ i1 ]) ? 2 : 0;
		if (2 == i0) {
			drawDialogTextField( canvas, nY );
		}
		for (; i0 < bars[ i1 ].length; i0 += 2) {
			int oTemplate = CcmTemplates.BYTES_PER_TEMPLATE
				* Arrays.binarySearch( UiDataSto.qTemplates.templateNamesFont, bars[ i1 ][ i0 ] );
			String txt = bars[ i1 ][ i0 + 1 ];
			if (oTemplate < 0) {
				++ oTemplate;
			}
			txt = ((0 < txt.length()) && (' ' > txt.charAt( 0 ))) ? ("" + kControlAsButton[ txt.charAt( 0 ) ]) : txt;
			if (4 != txt.length()) {
				mToolPaint.setARGB( 0xff,
					0xff & UiDataSto.qTemplates.templatesFont[ oTemplate + CcmTemplates.FONT_INX_COLOR + 1 ],
					0xff & UiDataSto.qTemplates.templatesFont[ oTemplate + CcmTemplates.FONT_INX_COLOR + 2 ],
					0xff & UiDataSto.qTemplates.templatesFont[ oTemplate + CcmTemplates.FONT_INX_COLOR + 3 ] );
				canvas.drawText( txt, nX, nY, mToolPaint );
			} else {
				mToolPaint2.setARGB( 0xff,
					0xff & UiDataSto.qTemplates.templatesFont[ oTemplate + CcmTemplates.FONT_INX_COLOR + 1 ],
					0xff & UiDataSto.qTemplates.templatesFont[ oTemplate + CcmTemplates.FONT_INX_COLOR + 2 ],
					0xff & UiDataSto.qTemplates.templatesFont[ oTemplate + CcmTemplates.FONT_INX_COLOR + 3 ] );
				canvas.drawText( txt.substring( 0, 2 ), nX, nY - eY / 3, mToolPaint2 );
				canvas.drawText( txt.substring( 2 ), nX, nY + eY / 3, mToolPaint2 );
			}
			nX += eX;
		}
		nX = UiDataSto.qWinXPixR / 3;
		nX += (i1 == 0) ? UiDataSto.WIN_X_MARGIN : nX;
		nX = (i1 == 2) ? UiDataSto.WIN_X_MARGIN : nX;
		nY += (i1 >= 2) ? (UiDataSto.qWinY1TopBarsPixR / 3) : 0;
		nY = (i1 > 3) ? nYBottom : nY;
	}
}

@Override
protected void onDraw( Canvas canvas ) {
//	CalcRpr.INSTANCE.wqRefreshNeeded = false;
//	CalcRpr.INSTANCE.qDoDraw = false;
//	Log.d( "onDraw", ":" ); //+ CalcRpr.INSTANCE.qTextLines ); //.lines[ 0 ] );
	canvas.drawColor( Color.WHITE );
	canvas.translate( 0, 0 );
	// Currently, no need to handle the details of the available QBatons:
	CalcPres.INSTANCE.getQPlace().flush( false );
	final Dib2Activity act = Dib2Activity.qActivity;
	if (null == act) {
		return;
	}
	if (CalcPres.wxExitRequest) {
		act.finish();
	}
	if (qResuming || ((900 > UiDataSto.qSwitches[ UiDataSto.SWI_STEP_OR_ACCESS_MINUTE ]) && CcmRunner.INSTANCE.isBusy())) {
		///// Other threads might still be active => show as busy.
		UiDataSto.qPageLinesFeed.lines = Dib2Local.kUiStepAcLoad;
		UiDataSto.qPageLinesFeed.count = Dib2Local.kUiStepAcLoad.length;
		drawTextLines( UiDataSto.qPageLinesFeed, 0xff, 0, canvas );
		if (qResuming) {
			act.triggerWorker( (char) 1, true );
		}
		qResuming = false;
		return;
	}
	CalcPres.INSTANCE.prepareTextLines( false );
	if (null == CcmRunner.qActive) {
		CalcPres.INSTANCE.consolidateTextLines();
	}

	for (int layerShift = 16; layerShift >= 0; layerShift -= 4) {
		boolean opaq = true; // 0xff/ 0xe0
		int red = 0;
		switch ((UiDataSto.qSwitches[ SWI_LAYERS ] >>> layerShift) & 0xf) {
			case SW_LAYER_KEYPAD_X:
				opaq = false;
			case SW_LAYER_KEYPAD:
				drawKeyPad( canvas, (opaq ? 0xff : 0x60), (opaq ? 0xa0 : 0xff) );
				break;
			case SW_LAYER_UPDATE:
				final CcmTextLines curr = UiDataSto.qPageLinesFeed;
				red = 0xff;
				if (null != curr) {
//					if (
//						//!canvasDone || 
//					(CalcRpr.INSTANCE.qPageLinesStable != curr) || (4 > layerShift)
//						|| (SW_LAYER_CANVAS != ((UiSwitches.qSwitches[ SWI_LAYERS ] >>> (layerShift - 4)) & 0xf))) {
					drawTextLines( curr, 0xff, red, canvas );
					break;
				}
			case SW_LAYER_CANVAS:
				final CcmTextLines last = UiDataSto.qPageLinesStable;
				if (null != last) {
					drawTextLines( last, 0xff, red, canvas );
//					canvasDone = true;
//					break;
				}
//				canvas.drawText( "" + CsvDb.instance.stackSize() + (last == null), 30, 300, mTextPaint );
				break;
			default:
				break;
		}
	}
	drawMarkers( canvas );
	drawToolBars( canvas );
}

public int getTextRow( int xYReal, int xRange ) {
	int nY = (3 * WIN_LINE_SPACING_ANDROID / 2) + (UiDataSto.qWinY1TopBarsPixR << UiDataSto.qShiftScale);
	nY = ((xYReal << UiDataSto.qShiftScale) - nY + WIN_LINE_SPACING_ANDROID / 2) / WIN_LINE_SPACING_ANDROID;
	return (0 > nY) ? 0 : ((nY >= xRange) ? (xRange - 1) : nY);
}

public int getTextColumn( int xXReal, String xText ) {
	if (null == xText) {
		return -1;
	}
	float[] widths = new float[ xText.length() ];
	mTextPaint.getTextWidths( xText, widths );
	int nX = -UiDataSto.qWinOffsetXPixR;
	for (int i0 = 0; i0 < xText.length(); ++ i0) {
		nX += widths[ i0 ];
		if (nX >= xXReal) {
			return i0;
		}
	}
	return xText.length();
}

public void setTouched4TextPos() {
	int nY = ((3 * WIN_LINE_SPACING_ANDROID / 2) >>> UiDataSto.qShiftScale);
	UiDataSto.qWinTouchedY3PixR[ 0 ] = nY
		+ ((UiDataSto.qPageLinesStable.textLineInx * WIN_LINE_SPACING_ANDROID) >>> UiDataSto.qShiftScale);
	mTextPaint.getTextBounds( UiDataSto.qPageLinesStable.lines[ UiDataSto.qPageLinesStable.textLineInx ], 0,
		UiDataSto.qPageLinesStable.textLineCharInx,
		zBounds );
	UiDataSto.qWinTouchedX3PixR[ 0 ] = zBounds.right;
}

//=====
}