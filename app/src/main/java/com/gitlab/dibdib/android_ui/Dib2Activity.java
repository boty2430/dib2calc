// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package com.gitlab.dibdib.android_ui;

import android.app.Activity;
import android.content.*;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;
import com.gitlab.dibdib.android_wk.*;
import com.gitlab.dibdib.common.TcvCodecAes;
import java.io.File;
import net.sf.dibdib.config.Dib2Config;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.StringFunc;
import net.sf.dibdib.thread_wk.CcmRunner;

public class Dib2Activity extends Activity {
//=====

static final String qDbFileName = "Dib2Calc.dm";
static Dib2Activity qActivity = null;

private static UiPresIf zPresenter = null;
private static File zSafeDir;
private Dib2View mView;

/** This part on the UI thread, the other parts on the thread created as WorkerTask.
 */
//=====
public class UiFuncNWkTrigger extends WorkerTask //AsyncTask< String, String, String > {
	implements UiFuncIf {
//=====

@Override
protected void onPostExecute( String result ) {
	super.onPostExecute( result );
	mView.invalidate();
}

@Override
protected void onProgressUpdate( String... values ) {
	super.onProgressUpdate( values );
	mView.invalidate();
}

@Override
public void toast( String msg ) {
	String t0 = msg;
	if (100 < msg.length()) {
		t0 = "Use 'Vw' to display larger contents ...\n" + msg.substring( 0, 35 ) + "...";
	}
	if (null != qActivity) {
		Toast.makeText( qActivity, t0, Toast.LENGTH_LONG ).show();
	}
}

@Override
public int getTextRow( int xYReal, int xRange ) {
	return mView.getTextRow( xYReal, xRange );
}

@Override
public int getTextColumn( int xXReal, String xText ) {
	return mView.getTextColumn( xXReal, xText );
}

@Override
public void setTouched4TextPos() {
	mView.setTouched4TextPos();
}

@Override
public boolean pushClipboard( String label, String text ) {
	ClipboardManager clipboard = (ClipboardManager) getSystemService( Context.CLIPBOARD_SERVICE );
	ClipData clip = ClipData.newPlainText( label, text );
	clipboard.setPrimaryClip( clip );
	return true;
}

@Override
public String getClipboard() {
	ClipboardManager clipboard = (ClipboardManager) getSystemService( Context.CLIPBOARD_SERVICE );
	if (!(clipboard.hasPrimaryClip())) {
		return null;
	}
	ClipData.Item item = clipboard.getPrimaryClip().getItemAt( 0 );
	return item.coerceToText( qActivity ).toString();
}

//=====
}

//=====

protected PlatformUtil mPlatform = new PlatformUtil();

public boolean init( String appShort, UiPresIf uiServer ) {
	zSafeDir = getFilesDir();
	zPresenter = uiServer;
	Dib2Config.init( 'A', appShort, Dib2Activity.qDbFileName, mPlatform, new TsvCodecIf[] { TcvCodecAes.instance } );
//	mWorkerAndroid = new WorkerPlatform();
	CcmRunner.INSTANCE.init( uiServer.getQPlace() ); //true, true );
//	if ("calc".equals( appShort ))
	zPresenter.init( CcmRunner.INSTANCE.wxGateIn ); //CcmRunnerDsp.INSTANCE.qGateOut, CcmRunnerDsp.INSTANCE.qGateIn ); //mWorkerAndroid );
	requestWindowFeature( Window.FEATURE_NO_TITLE );
	mView = new Dib2View( this );
	setContentView( mView );
	return true;
}

@Override
public void onResume() {
	super.onResume();
	qActivity = this;
	Log.d( "onResume", "." );
	zSafeDir = getFilesDir();
	Dib2Config.ui = new UiFuncNWkTrigger();
	mView.qResuming = true;
	zPresenter.start();
//	triggerWorker( (char) 1 );
}

@Override
public void onPause() {
	if (!zPresenter.stop( true )) {
		triggerWorker( StringFunc.SEND, true );
	}
	Dib2Config.ui = null;
	Log.d( "onPause", "." );
	qActivity = null;
	super.onPause();
}

public void triggerWorker( char key, boolean force ) {
//	if ((null != zPresenter.peek()) || force) {
	if ((null != CcmRunner.INSTANCE.wxGateIn.peek()) || force) {
		Log.d( "touch", "3x" + (int) key );
		((UiFuncNWkTrigger) Dib2Config.ui).cancel( false );
		Dib2Config.ui = new UiFuncNWkTrigger();
		((UiFuncNWkTrigger) Dib2Config.ui).execute(); // possibly queued
	}
}

//=====

///// Any thread.

public static void invalidate() {
	if (null != Dib2Config.ui) {
		// In this case it is okay, to use 'ui' for other threads:
		((UiFuncNWkTrigger) Dib2Config.ui).invalidate();
	}
}

public static File getSafeDir() {
	return zSafeDir;
}

public static Activity getActivity() {
	return qActivity;
}

//=====
}
