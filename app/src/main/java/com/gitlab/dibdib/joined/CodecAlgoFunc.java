// Copyright (C) 2016,2017,2019  Roland Horsch <gx work s{at}mai l.de> (and others).
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// Parts of this code are based on other people's ideas and snippets, as indicated in the methods.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package com.gitlab.dibdib.joined;

import java.security.*;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import javax.crypto.*;
import javax.crypto.spec.*;
import net.sf.dibdib.config.Dib2Config;
import net.sf.dibdib.thread_any.*;

public class CodecAlgoFunc {
//=====

/** PKCS7 or 5: 'AES/CBC/PKCS7Padding'/5 */
public static String INIT_cryptoAes_OLD = // 
// (Dib2Config.platform == '0') ? "AES/CBC/PKCS5Padding" : 
"AES/CBC/PKCS7Padding";
public static String INIT_cryptoPbkd = "PBKDF2WithHmacSHA1"; // "PBKDF2WithHmacSHA256";

/** FNV-1a, also for short keys. From UtilMisc ...
 * Cmp. http://www.isthe.com/chongo/tech/comp/fnv.
 */
private static long hash64_fnv1a( byte[] data ) {
	final long offs = 0xcbf29ce484222325L;
	long hash = offs;
	for (byte b0 : data) {
		hash ^= 0xff & b0;
		// hash *= prime;
		hash += (hash << 1) + (hash << 4) + (hash << 5) + (hash << 7) + (hash << 8) + (hash << 40);
	}
	return hash;
}

//See below.
private static void speck32_R( char[] yx, int iX, char k ) {
	yx[ iX ] = (char) ((yx[ iX ] >>> 7) | (yx[ iX ] << (16 - 7))); // ROR
	yx[ iX ] += yx[ 0 ];
	yx[ iX ] ^= k; // unsigned
	yx[ 0 ] = (char) ((yx[ 0 ] << 2) | (yx[ 0 ] >>> (16 - 2))); // ROL
	yx[ 0 ] ^= yx[ iX ]; // unsigned
}

//See below.
private static void speck32_RR( char[] yx, int iX, char k ) {
	yx[ 0 ] ^= yx[ iX ]; // unsigned
	yx[ 0 ] = (char) ((yx[ 0 ] >>> 2) | (yx[ 0 ] << (16 - 2))); // ROR
	yx[ iX ] ^= k; // unsigned
	yx[ iX ] -= yx[ 0 ];
	yx[ iX ] = (char) ((yx[ iX ] << 7) | (yx[ iX ] >>> (16 - 7))); // ROL
}

/** Cmp. https://en.wikipedia.org/wiki/Speck_(cipher) @20191101.
 * @param outBlock 4 bytes
 * @param dataBlock 4 bytes
 * @param key 8 bytes (big-endian)
 */
public static void speck32E( byte[] outBlock, int offset, byte[] dataBlock, long key ) {
	final int ROUNDS = 22;
	// char y = data[ 0 ]; char x = data[ 1 ]; char b = key[ 0 ]; char ax = key[ 3-x ];
	char[] yx = new char[] {
		(char) (((dataBlock[ 2 ] & 0xff) << 8) | (dataBlock[ 3 ] & 0xff)),
		(char) (((dataBlock[ 0 ] & 0xff) << 8) | (dataBlock[ 1 ] & 0xff)) };
	char[] ba2a1a0 = new char[] { (char) key, (char) (key >>> 48), (char) (key >>> 32), (char) (key >>> 16) };
	speck32_R( yx, 1, ba2a1a0[ 0 ] ); // R (x, y, b);
	int iA = 3;
	for (char i = 0; i < ROUNDS - 1; i ++) {
		speck32_R( ba2a1a0, iA, i ); //R(a, b, i);
		speck32_R( yx, 1, ba2a1a0[ 0 ] ); //R(x, y, b);
		iA = (3 == iA) ? 2 : ((2 == iA) ? 1 : 3);
	}
//	out[ 0 ] = yx[ 0 ]; out[ 1 ] = yx[ 1 ];
	outBlock[ offset ] = (byte) (yx[ 1 ] >>> 8);
	outBlock[ offset + 1 ] = (byte) (yx[ 1 ]);
	outBlock[ offset + 2 ] = (byte) (yx[ 0 ] >>> 8);
	outBlock[ offset + 3 ] = (byte) (yx[ 0 ]);
}

public static void speck32D( byte[] outBlock, int offset, byte[] dataBlock, long key ) {
	final int ROUNDS = 22;
	// char y = data[ 0 ]; char x = data[ 1 ]; char b = key[ 0 ]; char ax = key[ 3-x ];
	char[] yx = new char[] {
		(char) (((dataBlock[ 2 ] & 0xff) << 8) | (dataBlock[ 3 ] & 0xff)),
		(char) (((dataBlock[ 0 ] & 0xff) << 8) | (dataBlock[ 1 ] & 0xff)) };
	char[] ba2a1a0 = new char[] { (char) key, (char) (key >>> 48), (char) (key >>> 32), (char) (key >>> 16) };
	char[] expanded = new char[ ROUNDS ];
	int iA = 3;
	for (char i = 0; i < ROUNDS; i ++) {
		expanded[ i ] = ba2a1a0[ 0 ];
		speck32_R( ba2a1a0, iA, i ); //R(a, b, i);
		iA = (3 == iA) ? 2 : ((2 == iA) ? 1 : 3);
	}
	for (char i = 0; i < ROUNDS; i ++) {
		speck32_RR( yx, 1, expanded[ ROUNDS - 1 - i ] ); // R (x, y, b);
	}
//	out[ 0 ] = yx[ 0 ]; out[ 1 ] = yx[ 1 ];
	outBlock[ offset ] = (byte) (yx[ 1 ] >>> 8);
	outBlock[ offset + 1 ] = (byte) (yx[ 1 ]);
	outBlock[ offset + 2 ] = (byte) (yx[ 0 ] >>> 8);
	outBlock[ offset + 3 ] = (byte) (yx[ 0 ]);
}

/** Normalize/ honeyfy pass phrase. Somebody willing to check this? :-)
 */
public static byte[] toPass32( byte[] master, byte[] second, boolean usePrn ) {
	int len = (master.length >= 32) ? 32 : master.length;
	byte[] rand = new byte[ 65 ];

	System.arraycopy( master, 0, rand, 0, len );
	for (int count = len; count < 32; count += len) {
		rand[ count ++ ] = '.';
		System.arraycopy( master, 0, rand, count, len );
	}
	rand = Arrays.copyOf( rand, 32 );
	if (null == second) {
		return rand; // old
	}
	long key = hash64_fnv1a( rand );
	if (usePrn) {
		for (int i0 = 7; i0 >= 0; -- i0, ++ rand[ 3 ]) {
			speck32E( rand, i0 * 4, rand, key );
		}
	} else {
		new SecureRandom().nextBytes( rand );
	}

	byte[] honey = new byte[ 32 ];
	byte[] ascic = MiscFunc.asciiCompressed4Bytes( second );
	int cBits = (30 <= ascic.length) ? 30
		: ((15 <= ascic.length) || (0 != (rand[ 0 ] & 0x10))
			? ascic.length : (2 * ascic.length));
	int take = (cBits > ascic.length) ? 0x1 : 0xff;
	for (int i0 = 0; i0 < 32; ++ i0) {
		int rnd0 = (rand[ i0 ] & 0xff);
		int fill = (rnd0 & 0x80) | ((0 >= cBits) ? 0x80 : 0);
		fill = (0 != i0) ? fill : (0x80 & ~take);
		cBits -= (0 != fill) ? 0 : 1;
		honey[ i0 ] = (byte) ((rnd0 & 0x7f) | fill);
	}
	for (int i0 = 1; 0 < cBits; ++ i0) {
		if (0 != (honey[ i0 ] & 0x80)) {
			honey[ i0 ] &= 0x7f;
			-- cBits;
		}
	}
	len = 0;
	cBits = 1;
	for (int i0 = 0; i0 < 32; ++ i0) {
		if (0 == (honey[ i0 ] & 0x80)) {
			if (0 != (take & cBits)) {
				honey[ i0 ] = ascic[ len ++ ];
			}
			++ cBits;
		}
	}
	for (int i0 = 7; i0 >= 0; -- i0) {
		System.arraycopy( honey, i0 * 4, rand, 0, 4 );
		speck32E( honey, i0 * 4, rand, key );
	}
	return honey;
}

/** Decrypt encrypted pass phrase.
 * @param master Access code.
 * @param pass32 Encrypted.
 * @return Plain-text phrase.
 */
public static byte[] fromPass32( byte[] master, byte[] pass32 ) {
	int len = (master.length >= 32) ? 32 : master.length;
	byte[] out = new byte[ 65 ];
	byte[] prn = new byte[ 65 ];
	System.arraycopy( master, 0, prn, 0, len );
	for (int count = len; count < 32; count += len) {
		prn[ count ++ ] = '.';
		System.arraycopy( master, 0, prn, count, len );
	}
	prn = Arrays.copyOf( prn, 32 );
	long key = hash64_fnv1a( prn );
	for (int i0 = 7; i0 >= 0; -- i0) {
		System.arraycopy( pass32, i0 * 4, out, 0, 4 );
		speck32D( out, i0 * 4, out, key );
	}
	len = 0;
	int cBits = 1;
	int take = (0 == (out[ 0 ] & 0x80)) ? 0xff : 1;
	for (int i0 = 0; i0 < 32; ++ i0) {
		if (0 == (out[ i0 ] & 0x80)) {
			if (0 != (take & cBits)) {
				out[ len ++ ] = out[ i0 ];
			}
			++ cBits;
		}
	}
	return MiscFunc.bytes4AsciiCompressed( Arrays.copyOf( out, len ) );
}

public static String fingerprint( byte[] xPk, boolean asX509 ) {
	if (null == xPk) {
		return null;
	}
	byte[] pk = asX509 ? xPk : new X509EncodedKeySpec( xPk ).getEncoded();
	MessageDigest digest;
	try {
		// Waiting for standard format with SHA256 that includes address/ hash extension ...
		digest = MessageDigest.getInstance( "SHA1" );
	} catch (NoSuchAlgorithmException e) {
		return null;
	}
	digest.update( (byte) 0x99 );
	digest.update( (byte) (pk.length >> 8) );
	digest.update( (byte) pk.length );
	digest.update( pk );
	return StringFunc.hex4Bytes( digest.digest(), false );
}

/** @param ivSalt16 16 byte nonce used as salt for stretching and as IV (instead of '0'), with first byte != 0
 * @return encrypted data with prepended 16 byte salt */
//@SuppressLint( "Assert" )
public static byte[] encrypt_CBC( byte[] zippedDat, int from, int to,
	byte[] key, byte[] ivSalt16, int prepostLen ) throws Exception {
	SecretKeySpec aes = new SecretKeySpec( // getAesKeySpec( passphrase, ivSalt16 )
		key, "AES" );
	// PKCS7 or 5: 'AES/CBC/PKCS7Padding'/5
	Cipher cipher = Cipher.getInstance( INIT_cryptoAes_OLD );
	// '0' as IV would be enough for AES in this case, even though header of plain text is partly known:
	cipher.init( Cipher.ENCRYPT_MODE, aes, new IvParameterSpec( ivSalt16 ) ); // new byte[16]));
	//assert Arrays.equals( ivSalt16, cipher.getParameters().getParameterSpec( IvParameterSpec.class ).getIV() );
	byte[] enc = cipher.doFinal( zippedDat, from, to - from );
	byte[] out = Arrays.copyOf( ivSalt16, ivSalt16.length + enc.length + 2 * prepostLen );
	if (0 < prepostLen) {
		System.arraycopy( ivSalt16, 0, out, prepostLen, ivSalt16.length );
	}
	System.arraycopy( enc, 0, out, ivSalt16.length + prepostLen, enc.length );
	return out;
}

/** @param ivSalt nonce used as salt for stretching and IV if first byte != 0 */
public static byte[] decrypt_CBC( byte[] dat, int from, int to,
	byte[] key, byte[] ivSalt ) throws Exception {
	byte[] iv = new byte[ 16 ];
	iv = Arrays.copyOfRange( ivSalt, 0, 16 );
	SecretKeySpec aes = new SecretKeySpec( //getAesKeySpec( passphrase, iv ), 
		key, "AES" );
	Cipher cipher = Cipher.getInstance( INIT_cryptoAes_OLD );
	cipher.init( Cipher.DECRYPT_MODE, aes, new IvParameterSpec( iv ) );
	// Check block length:
	if (0 != (to - from) % 16) {
		Dib2Config.log( "decrypt", "AES?/ bad block length?" );
		to -= (to - from) % 16;
	}
	byte[] out = cipher.doFinal( dat, from, to - from );
	return out;
}

public static byte[] mac( byte[] xKey32, byte[] xEncryptedData ) throws Exception {
	SecretKey hmacKey = new SecretKeySpec( xKey32, "HmacSHA256" );
	Mac mac = Mac.getInstance( "HmacSHA256" );
	mac.init( hmacKey );
	return mac.doFinal( xEncryptedData );
}

//=====
}
