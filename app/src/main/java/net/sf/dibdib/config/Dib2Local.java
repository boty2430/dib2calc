// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.config;

public interface Dib2Local {
//=====

public static final String[] kLanguages = { "EN", "DE" };
//public static final int kStepIntro = 640;

public static final String[] kWelcome_CALC = {
	"Dib2Calc (a.k.a. Dibdib Calculator).",
	"Dib2Calc (genannt Dibdib Calculator).",
};

public static final String[] kLicensePre = {
	"",
	"(Lizenz: Nur auf Englisch. Im Zweifelsfall Programm nicht benutzen!)",
};

// Initial one:
public static final String[] kUiStep0 = {
	Dib2Constants.NO_WARRANTY[ 0 ],
	"",
	"EN: Press the green '>' (bottom) to start",
	"      the introduction. Afterwards you can",
	"      switch the language with LA-NG.",
	"DE: Mit dem Schalter '>' (unten) wird die",
	"      Einfuehrung gestartet. Danach wird",
	"      die Sprache mit LA-NG umgeschaltet.",
	"",
	"ES / FR / ... ",
	"(Somebody willing to help translate?)",
	"",
};

public static final String[] kUiAgree = {
	"",
	"",
	//
	"$WELCOME",
	"$WELCOME",
	//
	"",
	"",
	//
	"This program comes with",
	"Bitte beachten: 'This program comes",
	//
	"ABSOLUTELY NO WARRANTY.",
	"with ABSOLUTELY NO WARRANTY.'",
	//
	"Do not use this program if you",
	"Benutzen Sie das Programm nicht",
	//
	"do not agree to that in its",
	"ohne den englischen Text zu",
	//
	"fullest possible sense.",
	"verstehen und voll zuzustimmen!",
	//
	"",
	"",
	//
	"Press '>' (again).",
	"'>' (nochmal) druecken.",
	//
	"",
	"",
};

public static final String[] kUiStepAcStart = {
	"",
	"",
	Dib2Constants.NO_WARRANTY[ 0 ],
	"",
	"EN: Press the green '>' (bottom).",
	"DE: Gruenes '>' (unten) druecken.",
	"",
	"",
};

public static final String[] kUiStepAcLoad = {
	"",
	"",
	Dib2Constants.NO_WARRANTY[ 0 ],
	"",
	"",
	"",
	"EN: Loading data ...",
	"DE: Daten werden geladen ...",
	"",
	"",
};

public static final String[] kUiStepAc = {
	"",
	"",
	Dib2Constants.NO_WARRANTY[ 0 ],
	"",
	"EN: Please enter your access code and",
	"      press the green '>' (bottom).",
	"DE: Bitte Zugangscode eingeben ('access code')",
	"      und gruenes '>' (unten) druecken.",
	"",
	"",
};

public static final String[] kUiStepAcWait = {
	"",
	"",
	Dib2Constants.NO_WARRANTY[ 0 ],
	"",
	"EN: Please wait, then enter your access code",
	"      and press the green '>' (bottom).",
	"DE: Bitte warten, dann Zugangscode eingeben",
	"      und gruenes '>' (unten) druecken.",
	"",
	"",
};

public static final String[] kUiStepPw = {
	"",
	"",
	Dib2Constants.NO_WARRANTY[ 0 ],
	"",
	"EN: Please enter your password and",
	"      press the green '>' (bottom).",
	"DE: Bitte Passwort eingeben",
	"      und gruenes '>' (unten) druecken.",
	"(Note: '0' + ESCAPE (top left) deletes the file data)",
	"(Anm.: '0' + ESCAPE (oben links) loescht die Daten)",
	"",
	"",
};

public static final String[] kUiStep600x = {
	//
	"",
	"",
	//
	"EN: Press '>' (above on the right",
	"DE: Fortsetzen mit '>'",
	//
	"       or bottom) to continue.",
	"       (oberhalb rechts oder unten)",
	//
	"",
	"",
	//
	"EN: Switch language with LA-NG.",
	"EN: Switch language with LA-NG.",
	//
	//
	"DE: Sprache mit LA-NG umschalten.",
	"DE: Sprache mit LA-NG umschalten.",
	//
	"",
	"",
};

public static final String[] kUiStep610 = {
	"",
	"",
	//
	"EN: The LA-NG (top left) button",
	"DE: LA-NG (oben links) schaltet die",
	//
	"      switches the language.",
	"      Sprache um.",
	//
	// A long click switches the keyboard.",
	// Tastatur umschalten mit langem Klick.",
	//
	"      (Above that: ESC and ZOOM buttons).",
	"      (Oberhalb: ESC und ZOOM).",
	//
	"",
	"",
	//
	"      Press '>' (bottom) to continue.",
	"      Mit '>' (unten) fortsetzen.",
	//
	"",
	"",
	//
	"DE: Mit LA-NG (oben links) Sprache",
	"EN: Use LA-NG (top left) to switch",
	//
	"      umschalten.",
	"      the language.",
};

public static final String[] kUiStep620 = kUiAgree;

public static final String[] kUiStep630 = {
	//
	"(INTRODUCTION:)",
	"(EINFUEHRUNG:)",
	//
	"Press '+' (below) to add 2 and 3:",
	"'+' (unterhalb) druecken, um 2 und 3",
	//
	"X=2, Y=3 as shown, then with the '+':",
	"zu addieren: X=2, Y=3 wie angezeigt,",
	//
	"(Y + X) => [3 2 +] = 5",
	"dann mit '+': (Y + X) => [3 2 +] = 5.",
	//
	"",
	"",
	//
	"NOTE:",
	"ANMERKUNG:",
	//
	"ESC (red button top left)",
	"ESC (roter Schalter oben",
	//
	"skips introduction.",
	"ueberspringt Einfuehrung.",
	//
	"ZOOM buttons (next to it)",
	"ZOOM Schalter (daneben)",
	//
	"adjust the text size.",
	"aendern die Textgroesse.",
	//
	"",
	"",
};

public static final String[] kUiStep640 = {
	//
	"",
	"",
	//
	"Press '6' and",
	"'6' und 'Enter'",
	//
	"'Enter'",
	"(unten,",
	//
	"(bottom right,",
	"oberhalb 'OK')",
	//
	"above 'OK') to",
	"druecken, um",
	//
	"push a new value.",
	"einen Wert einzufügen.",
	//
	"",
	"",
};

public static final String[] kUiStep650 = {
	//
	"",
	"",
	//
	"Press '9' and",
	"'9' und '\u00b1' druecken",
	//
	"'\u00b1' for a negative",
	"fuer einen negativen",
	//
	"value.",
	"Wert.",
	//
	"",
	"",
};

public static final String[] kUiStep660 = {
	//
	"",
	"",
	//
	"Press '*' to multiply the",
	"'*' druecken, um die ersten zwei",
	//
	"first 2 values.",
	"Werte zu multiplizieren.",
	//
	"",
	"",
};

public static final String[] kUiStep670 = {
	//
	"",
	"",
	//
	"Press '*' again.",
	"'*' nochmal druecken.",
	//
	"",
	"",
	//
	"NOTE:",
	"ANMERKUNG:",
	//
	"Use 'GO' for operator names, e.g.:",
	"Funktionsnamen anwenden mit 'GO':",
	//
	"Instead of '+' you could type 'ADD'",
	"Anstatt '+' kann auch 'ADD' eingegeben",
	//
	"and then press GO (top bar).",
	"werden, gefolgt von dem GO Schalter",
	//
	"",
	"(in Leiste oben)",
	//
	//
	"",
	"",
};

//=====
}
