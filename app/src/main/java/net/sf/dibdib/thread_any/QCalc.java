// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_any;

import java.util.*;
import java.util.regex.*;
import net.sf.dibdib.generic.QResult;
import net.sf.dibdib.thread_any.QMapSto.QVal;

/** Operators as functions (on Strings, Doubles, bitlists, lists, ...).
 * First 4 char's must be significant. First string of optional symbols is empty if
 * operator symbol is the default, rather than the name.
 * Boolean value = lowest bit of bitlist: TRUE = -1 or odd number, FALSE = 0 or even number.
 * Bitlist functions operate bitwise and are thus also used for booleans.
 */
public enum QCalc {
//=====

//TODO Consider number of return values for QVal.IN_PROCESS

///// Math (atomic)

ABS( 1, "POSV", "absolute value" ),
ACOS( 1, "acos" ),
ADD( 2, "\u2214", "sum of 2 values (each for sequences)" ),
AND( 2, "", "&", "&&", "\u2227", "binary AND" ),
ASIN( 1, "asin" ),
ATAN( 1, "atan" ),
CEIL( 1, "integer above" ),
COMP( 2, "comparison: Y X -> -1/0/1" ),
COND( 2, "conditional value" ),
COS( 1, "cos" ),
COSH( 1, "cosh" ),
DEG( 1, "radians to degrees" ),
DIV( 2, "\u00f7", "\u2215", "division for 2 values (each)" ),
E( 0, "Euler" ),
FACTORIAL( 1, "", "!!", "factorial" ),
FALSE( 0, "0", "return 0 as even value (= boolean FALSE)" ),
FLOOR( 1, "integer below" ),
FRAC( 1, "fractional part" ),
GRAD( 1, "radians to gradiens" ),
I,
IMPLIES,
INF( 0, "infinity" ),
INT( 1, "non-fractional part" ),
LIMIT,
LOG( 1, "base 10 logarithm" ),
LN( 1, "natural logarithm" ),
MAX( 2, "maximum" ),
MIN( 2, "minimum" ),
MOD( 2, "+%", "modulo (positive remainder)" ),
MUL( 2, "\u00d7", "\u2217", "multiply: product of 2 values (each)" ),
NADD( 1, "", "!+", "\u2213", "\u00b1", "additive inverse, change of sign" ),
NAN( 0, "error value NaN (not a number)" ),
NAND( 2, "!&", "\u22bc", "\u2206", "binary NAND" ),
NEGV, //( 1, "", "\u00af", "negative value [ABS !+]" ),
NMULT( 1, "", "!*", "\u215f", "\u00b9", "multiplicative inverse, reciprocal" ),
NOR( 2, "!|", "binary NOR" ),
NOT( 1, "!", "\u00ac", "negated bits, binary NOT" ),
OR( 2, "", "|", "||", "\u2228", "binary OR" ),
PERCENT( 1, "", "%", "percentage value (/100)" ),
PI( 0, "\u03c0", "pi" ),
POWER( 2, "", "**", "power" ), // Do not use '^': => variables/ mappings.
PRED( 1, "predecessor" ),
RADD( 1, "RAD", "radians, from degrees" ),
RADG( 1, "radians, from gradiens" ),
RANDOM,
ROOT,
ROUND( 1, "rounded value" ),
SIGN( 1, "value -> -1/0/1" ),
SIN( 1, "sin" ),
SINH( 1, "sinh" ),
SHL( 2, "", "<<", "\u226a", "\u00ab", "binary shift left" ),
SHR( 2, "", ">>", "\u226b", "\u00bb", "binary shift right" ),
SQRT( 1, "square root" ),
SQUARE( 1, "squared value" ),
SUB( 2, "\u2212", "subtraction with 2 values (each)" ),
SUCC( 1, "successor" ),
TAN( 1, "tan" ),
TANH( 1, "tanh" ),
TRUE( 0, "-1", "return -1 as odd value (= boolean TRUE)" ),
XOR( 2, "|%", "\u22bb", "\u2207", "binary XOR" ),

///// Any atomic type

DATI( 1, "date, from integer representation" ),
DATSL( 1, "convert 'slot second' to date" ),
DATMS( 1, "convert msec to date" ),
EQ( 2, "", "=", "==", "is equal" ),
GE( 2, "", ">=", "\u2265", "is greater than or equal" ),
GT( 2, "", ">", "is greater than" ),
IS, // ( 2, "is same type (compare type of 2 values)" ),
IDAT( 1, "integer representation of date" ),
ISNT,
LE( 2, "", "<=", "\u2264", "is less than or equal" ),
LEN,
LT( 2, "", "<", "is less than" ),
MINUS( 2, "", "-", "subtract value or char's" ),
MSEC,
NE( 2, "", "!=", "<>", "\u2260", "is not equal" ),
NOP( 0, 0, "no operation" ),
OPT, // ( 3, "CHOICE", "optional value, choice: Z Y Xbool -> Z|Y" ),
PLUS( 2, "", "+", "add value or char's" ),
RGX, //( 2, "", "~", "index of match for regular expression" ),
RGXG, //( 3, "", "~/", "n-th group of match for regular expression" ),
RGXN, //( 3, "", "~+", "index of n-th match for regular expression" ),
RGXT, //( 3, "", "~&", "n-th match for regular expression" ),
SLSD, // date to slot second
SPLIT( 2, "", "/", "RGXS", "divide or split, by value or pattern" ),
TIMES( 2, "", "*", "multiply or repeat, by value" ),
UNION,
UTF16,
UTF32,
UTF8,

///// Sequence/ vector/ matrix

ALL, // ( -2, "return -1 if all members match P: [L] P -> Xbool" ),
AT, // (-2, "", ".", "INDX"
ATKEY, // ".:" associative
CONC, // ( -2, "", ",", "CONS", "concatenate" ),
CUT, // ( -2, "split list at member n: [L] n -> [L0] [L1]" ),
FOLD, // L/R
DUP( -1, 2, "duplicate: X -> X X" ),
EDUPTO, // ( -2, "DUPI", "duplicate to: ... X I -> X ... X" ),
ELEMENT, //( -2, "HAS", "is member of: X [L] -> Ybool" ),
EMAP, // ( -2, "operator X (with additional args) as scalar on each member: ... X [L] -> [L']" ),
EMPTY,
EPICK, // dup item n to top ... ->
EROLL, // move item n to top ... ->
EXPAND,
FILTER, // ( -2, "filter list with pattern P: [L] P -> [L']" ),
INTERSECTION,
IN,
//INDEX, => AT
MDROP, // ( -2, "drop first/last n members in list: [L] n -> [L']" ),
MCROSS, // cross product
//MDIA, matrix with matching diagonal
//MDIAG, // diagonal of matrix
MDIV( -2, "matrix division" ),
MDOT, // scalar product
MEAN,
MEDIAN,
MEIG, // eigenvalues
MEYE, // identity matrix
//MFLIP,
MINV( -2, "matrix inversion" ),
//MINP pseudo-inverse
MMUL( -2, "matrix multiplication" ),
MODE, // modal
MONE, // matrix with n ones
MROT, // ( 1, "rotate matrix" ),
MSTD, // standard deviation
MTRP, // .' ( 1, "transpose matrix" ),
//MTRX  ..'
MZERO, // matrix with n zeros
NIP, // drop second
OF,
ONEOF, // ( -2, "return -1 if exactly one member matches P: [L] P -> Xbool" ),
OVER, // dup second to top
PRODUCT, // ( 1, "\u220f", "product of sequence of values" ),
RANGE, // ".." (1, "end/ start end/ start end step
REDUCE,
RHO,
RLDOWN, // ( -3, "roll down: X Y Z -> Y Z X" ),
RLUP, // ( -3, "roll up: X Y Z -> Z X Y" ),
ROT, // ( -3, "ROTATE", "rotate: X Y Z -> Z Y X" ),
SELECT, // ( 2, "compress list by list of booleans" ),
SEQ, // ( 2, "create running sequence" ),
SMALL, // ( -1, "is empty list or single value" ),
SOME, // ( -2, "return -1 if some members match P: [L] P -> Xbool" ),
SORT,
SUBSET,
SUPERSET,
SUM, // ( 1, "\u2211", "sum of sequence of values" ),
SWAP( -2, 2, "swap top 2 values: Y X -> X Y" ),
TAIL, // ( 2, "tail recursion" ),
TAKE, // ( -2, "take first n members: [L] n -> [L']" ),
TO, // ".="
TOKEY, // ".!" associative
TUCK, // dup below, top to third
SIZE,
Z, //(1, "`", "quote: turn sequence into atomic string (literal item)"),
ZRX( -2, "", "`~", "index of match for regular expression" ),
ZRXG( -3, "", "`/", "n-th group of match for regular expression" ),
ZRXN( -3, "", "`+", "index of n-th match for regular expression" ),
ZRXT( -3, "", "`&", "n-th match for regular expression" ),

///// External (not pure) 0: generic

ABOUT( 0, 0, "show license" ),
CLEAR( -1, 0, "CLR", "DROP", "POP", "\u0008", "\u00a2", "drop top value" ),
CLR1( -1, 0, "drop top value" ),
CLR2( -1, 0, "drop top 2 values" ),
CLR3( -1, 0, "drop top 3 values" ),
CLRALL( 0, 0, "clear all 'volatile' data (stack + memory)" ),
CLOSE,
DATE( 0, "current date" ),
// EX* has to consume the stack ...
EXDIP, // ( 2, "execute and reverse: X [P] -> R X" ),
EXDUP, // ( 1, "execute and save: [P] -> [P] R" ),
EXEC( -1, -1, "GO", "execute (taking additional args): ... [P] -> R" ),
EXISTS,
EXIT( 0, "QUIT", "end program" ),
EXMAP, // ( 2, "execute on each member: ... [P] [L] -> [L']" ),
EXOPT, // ( 3, "execute branch: ... [P0] [P1] Xbool -> R" ),
EXWHILE, // ( 2, "execute D while C: ... [D] [C] -> R" ),
FDECR( -1, "Decrypt to file, from .dib" ),
FENCR( -1, "Encrypt file as .dib" ),
FORALL,
HELP( 0, 0, "show help page" ),
LOAD( -1, "@", "MR", "load value from memory/ URI: name -> val" ),
MC( -1, "MMC", "CLRM", "clear memory value" ),
MMAT, // get variable's N-th subvalue
MMCA, // ( 0, "clear memory (all values)" ),
MMKL, // get value for key within variable
MMKS, // put value for key into variable
MMTO, // put variable as variable's N-th subvalue
OPEN,
READ,
SEEK,
STORE( -2, ":", "MS", "DEF", "store/ post value in memory/ to URI: val name -> val/NaN" ),
TICK( 0, "CLOCK", "current time in msec" ),
UICOD( -1, "Set UI offset for Unicode characters" ),
WRITE,

///// External 1: mappings

ARCHIVE, //( 0, "archive old entries" ),
DUMP( 0, /* "LSALL",*/"display all" ),
EXPALL( -1, "export all data (incl. keys!) as plain CSV (careful!)." ),
EXPORT( -1, "export data as plain CSV to file etc." ),
IMPORT( -1, "import data from file etc." ),
PW( -1, "set overall password" ),
PWAC( -1, "set app's access code" ),
QAT, // "^."
QATKEY, // "^:"
QCAT( -2, "change mapping's categories: OID cats ->" ),
QDEL( -1, "QCLR", "delete mapping for given OID" ),
QFILTER( 0, "switch category for filtering" ),
QLOAD( -1, "get data for given OID" ),
QOID( -1, "get first OID for label and current cat: name -> OID" ),
QNEXT( -1, "get next OID for same label" ),
QQL( -1, "@^", "get data for mapping's label and current cat" ),
QSTORE( -3, "store new mapping: data cats label ->" ),
QS( -2, ":^", "store new mapping for current cats: data label ->" ),
QTO,
QTOKEY,
QUP( -2, "update/ replace data of mapping: OID data ->" ),
SAVTO( -1, "save all data as encoded copy to named file" ),
VIEW, //( 1, "set view (filter/ category)" ),
VWCAT( -1, "set cat for viewing data" ),
// VWFILTER,

/////,
;

public final int cArgs;
//public final boolean asSequences;
public final int cReturnValues;
public final String[] optionals;
public final String description;

private static char[] functSymbols;
private static QCalc[] functEnums;

/////

static {
	TreeMap< Integer, QCalc > map = new TreeMap< Integer, QCalc >();
	for (QCalc funct : QCalc.values()) {
		for (String opt : funct.optionals) {
			if (1 == opt.length()) {
				map.put( opt.charAt( 0 ) & 0xffff, funct );
			}
		}
	}
	functSymbols = new char[ map.size() ];
	functEnums = new QCalc[ functSymbols.length ];
	int cnt = 0;
	for (Integer ch : map.keySet()) {
		functSymbols[ cnt ] = (char) (int) ch;
		functEnums[ cnt ++ ] = map.get( ch );
	}
}

private QCalc() {
	cArgs = -1;
//	asSequences = false;
	cReturnValues = 0;
	optionals = new String[ 0 ];
	description = null;
}

private QCalc( int xcArgs, int xcReturnValues, String... xmOptionals ) {
	cArgs = (0 <= xcArgs) ? xcArgs : -xcArgs;
//	asSequences = (0 > xcArgs);
	cReturnValues = xcReturnValues;
	optionals = ((null != xmOptionals) && (1 <= xmOptionals.length)) ? Arrays.copyOf( xmOptionals, xmOptionals.length - 1 ) : xmOptionals;
	description = ((null != xmOptionals) && (1 <= xmOptionals.length)) ? (xmOptionals[ xmOptionals.length - 1 ]) : null;
}

private QCalc( int xcArgs, String... xmOptionals ) {
	cArgs = (0 <= xcArgs) ? xcArgs : -xcArgs;
//	asSequences = (0 > xcArgs);
	cReturnValues = 1;
	optionals = ((null != xmOptionals) && (1 <= xmOptionals.length)) ? Arrays.copyOf( xmOptionals, xmOptionals.length - 1 ) : xmOptionals;
	description = ((null != xmOptionals) && (1 <= xmOptionals.length)) ? (xmOptionals[ xmOptionals.length - 1 ]) : null;
}

public static QCalc getOperator( String cmd ) {
	if ((null == cmd) || (0 >= cmd.length())) {
		return null;
	}
	QCalc funct = null;
	try {
		funct = QCalc.valueOf( StringFunc.toUpperCase( cmd ) );
	} catch (Exception e) {
		funct = null;
	}
	if ((null == funct) && (1 == cmd.length())) {
		final char c0 = cmd.charAt( 0 );
		int iSym = Arrays.binarySearch( functSymbols, c0 );
		if (0 <= iSym) {
			funct = functEnums[ iSym ];
		}
	}
	if (null == funct) {
		for (QCalc fx : QCalc.values()) {
			for (int i0 = fx.optionals.length - 1; i0 >= 0; -- i0) {
				if (fx.optionals[ i0 ].equals( cmd )) {
					funct = fx;
					break;
				}
			}
		}
	}
	if (null == funct) {
		final String cmdU = StringFunc.toUpperCase( cmd );
		for (QCalc fx : QCalc.values()) {
			final String nam = fx.name();
			if (((3 <= nam.length()) && cmdU.startsWith( nam ))
				|| ((4 <= cmdU.length()) && nam.startsWith( cmdU ))) {
				funct = fx;
				break;
			}
		}
	}
	return funct;
}

/////

public String getOperator() {
	return ((1 < optionals.length) && (0 >= optionals[ 0 ].length())) ? optionals[ 1 ] : null;
}

public String getOperatorOrName() {
	return ((1 < optionals.length) && (0 >= optionals[ 0 ].length())) ? optionals[ 1 ] : name();
}

public String getDescription() {
	StringBuilder out = new StringBuilder( 100 );
	if (null == description) {
		out.append( '.' );
	}
	out.append( name() + " (" + cArgs + ")  \t" );
	for (String opt : optionals) {
		if (0 < opt.length()) {
			out.append( " " + opt + ' ' );
		}
	}
	if (null != description) {
		out.append( "\t" + description );
	}
	return out.toString();
}

/** Handle numeric cases. Return null if n.a. */
public double[] calc( double[] arguments ) {
	if (cArgs != arguments.length) {
		return null;
	}
	double out0 = Double.NaN;
	double out1 = Double.NaN;
	int cOut = 1;
	try {
		long vx = 0;
		switch (this) {
			case ABS:
				out0 = (0 > arguments[ 0 ]) ? (-arguments[ 0 ]) : arguments[ 0 ];
				break;
			case ACOS:
				out0 = Math.acos( arguments[ 0 ] );
				break;
			case ADD:
				out0 = arguments[ 0 ] + ((1 == cArgs) ? 1 : arguments[ 1 ]);
				break;
			// case ALL:
			case AND:
				// case '\u2227':
				vx = (long) arguments[ 0 ] & (long) arguments[ 1 ];
				out0 = vx;
				break;
			case ASIN:
				out0 = Math.asin( arguments[ 0 ] );
				break;
			// case AT:
			case ATAN:
				out0 = Math.atan( arguments[ 0 ] );
				break;
			case CEIL:
				out0 = Math.ceil( arguments[ 0 ] );
				break;
//			case CLEAR:
//				// case '\u00a2':
//				return new double[ 0 ];
			case COMP:
				out0 = (arguments[ 0 ] == arguments[ 1 ]) ? 0.0 : ((arguments[ 0 ] > arguments[ 1 ]) ? 1.0 : -1.0);
				break;
			// case CONC:
			// case COND:
			case COS:
				out0 = Math.cos( arguments[ 0 ] );
				break;
			case COSH:
				out0 = Math.cosh( arguments[ 0 ] );
				break;
			// case CROSS:
			// case CUT:
			// case DATE:
			case DEG:
				out0 = arguments[ 0 ] * 180.0 / Math.PI;
				break;
			case DIV:
				// case '\u00f7':
				out0 = arguments[ 0 ] / arguments[ 1 ];
				break;
			// case DROP:
			case DUP:
				out0 = arguments[ 0 ];
				out1 = out0;
				cOut = 2;
				break;
			case E:
				out0 = Math.E;
				break;
			// case ELEMENT:
			// case EMPTY:
			case EQ:
				out0 = (arguments[ 0 ] == arguments[ 1 ]) ? -1.0 : 0.0;
				break;
			case EXDIP:
			case EXDUP:
			case EXEC:
			case EXMAP:
			case EXOPT:
				return null; // external
//			case EXISTS:
//			case EXPAND:
			case EXWHILE:
				return null; // external
			case FACTORIAL:
				vx = (long) arguments[ 0 ];
				if (200 >= vx) {
					out0 = vx;
					for (int i0 = (int) vx - 1; i0 >= 2; -- i0) {
						out0 *= i0;
					}
				} else {
					out0 = Double.NaN;
				}
				break;
			case FALSE:
				out0 = 0.0;
				break;
			// case FILTER:
			case FLOOR:
				out0 = Math.floor( arguments[ 0 ] );
				break;
			// case FORALL:
			case FRAC:
				vx = (long) arguments[ 0 ];
				out0 = arguments[ 0 ] - vx;
				break;
			case GE:
				out0 = (arguments[ 0 ] >= arguments[ 1 ]) ? -1.0 : 0.0;
				break;
			// case GET:
			case GRAD:
				out0 = arguments[ 0 ] * 200.0 / Math.PI;
				break;
			case GT:
				out0 = (arguments[ 0 ] > arguments[ 1 ]) ? -1.0 : 0.0;
				break;
			// case HELP:
			// case IS:
			// case I:
			// case IMPLIES:
			// case IN:
			case INF:
				out0 = Double.POSITIVE_INFINITY;
				break;
			// case INDEX:
			case INT:
				vx = (long) arguments[ 0 ];
				out0 = vx;
				// case INTERSECTION:
				// case ISNT:
			case LE:
				out0 = (arguments[ 0 ] <= arguments[ 1 ]) ? -1.0 : 0.0;
				break;
			// case LIMIT:
			case LOG:
				out0 = Math.log10( arguments[ 0 ] );
				break;
			case LN:
				out0 = Math.log( arguments[ 0 ] );
				break;
			case LT:
				out0 = (arguments[ 0 ] < arguments[ 1 ]) ? -1.0 : 0.0;
				break;
			// case MAP:
			case MAX:
				out0 = (arguments[ 0 ] >= arguments[ 1 ]) ? arguments[ 0 ] : arguments[ 1 ];
				break;
			// case MDIV:
			// case MINV:
			// case MMUL:
			case MIN:
				out0 = (arguments[ 0 ] <= arguments[ 1 ]) ? arguments[ 0 ] : arguments[ 1 ];
				break;
			case MOD:
				out0 = arguments[ 0 ] % arguments[ 1 ];
				break;
			// case MROT:
			// case MSEC:
			// case MTRP:
			case MUL:
				out0 = arguments[ 0 ] * arguments[ 1 ];
				break;
			case NADD:
				// case '_':
				// case '\u2213':
				// case '\u00b1':
				out0 = -arguments[ 0 ];
				break;
			case NAN:
				out0 = Double.NaN;
				break;
			case NAND:
				// case '\u22bc':
				// case '\u2206':
				vx = ~((long) arguments[ 0 ] & (long) arguments[ 1 ]);
				out0 = vx;
				break;
			case NE:
				out0 = (arguments[ 0 ] != arguments[ 1 ]) ? -1.0 : 0.0;
				break;
			case NMULT:
				// case ';':
				// case '\u215f':
				// case '\u00b9':
				out0 = 1.0 / arguments[ 0 ];
				break;
			case NOP:
				return new double[ 0 ];
			case NOR:
				vx = ~((long) arguments[ 0 ] & ~(long) arguments[ 1 ]);
				out0 = vx;
				break;
			case NOT:
				vx = ~(long) arguments[ 0 ];
				out0 = vx;
				break;
			// case OF:
			// case ONEOF:
			// case OPT:
			case OR:
				// case '\u2228':
				vx = (long) arguments[ 0 ] | (long) arguments[ 1 ];
				out0 = vx;
				break;
			case PERCENT:
				out0 = arguments[ 0 ] / 100.0;
				break;
			case PI:
				out0 = Math.PI;
				break;
			case POWER:
				out0 = Math.pow( arguments[ 0 ], arguments[ 1 ] );
				break;
			case PRED:
				vx = (long) arguments[ 0 ];
				out0 = vx - 1;
				break;
			// case PRODUCT:
			// case PUT:
			case RADD:
				out0 = arguments[ 0 ] * Math.PI / 180.0;
				break;
			case RADG:
				out0 = arguments[ 0 ] * Math.PI / 200.0;
				break;
			// case RANDOM:
			// case REDUCE:
			// case RHO:
			// case RLDOWN:
			// case RLUP:
			// case ROOT:
			// case ROTATE:
			case ROUND:
				out0 = Math.round( arguments[ 0 ] );
				break;
			// case SELECT:
			// case SEQ:
			case SIGN:
				out0 = (0.0 < arguments[ 0 ]) ? 1.0 : ((0.0 > arguments[ 0 ]) ? -1.0 : 0.0);
				break;
			case SIN:
				out0 = Math.sin( arguments[ 0 ] );
				break;
			case SINH:
				out0 = Math.sinh( arguments[ 0 ] );
				break;
			// case SIZE:
			case SHL:
				vx = (long) arguments[ 0 ] << (long) arguments[ 1 ];
				out0 = vx;
				break;
			case SHR:
				vx = (long) arguments[ 0 ] >> (long) arguments[ 1 ];
				out0 = vx;
				break;
			// case SMALL:
			// case SOME:
			//case SORT:
			// case SPLIT:
			case SQRT:
				out0 = Math.sqrt( arguments[ 0 ] );
				break;
			case SQUARE:
				out0 = arguments[ 0 ] * arguments[ 0 ];
				break;
			case SUB:
				out0 = arguments[ 0 ] - ((1 == cArgs) ? 1 : arguments[ 1 ]);
				break;
			// case SUBSET:
			case SUCC:
				vx = (long) arguments[ 0 ];
				out0 = vx + 1;
				break;
			// case SUM:
			// case SUPERSET:
			case SWAP:
				out0 = arguments[ 1 ];
				out1 = arguments[ 0 ];
				cOut = 2;
				break;
			// case TAIL:
			// case TAKE:
			case TAN:
				out0 = Math.tan( arguments[ 0 ] );
				break;
			case TANH:
				out0 = Math.tanh( arguments[ 0 ] );
				break;
			// case TICK:
			case TRUE:
				out0 = -1.0;
				break;
			// case UNION:
			case XOR:
				// case '\u22bb':
				// case '\u2207':
//				long vx = (((long) vals[ 0 ]) & ((1L << 48) - 1)) ^ (((long) vals[ 1 ]) & ((1L << 48) - 1));
				vx = (long) arguments[ 0 ] ^ (long) arguments[ 1 ];
				out0 = vx;
				break;
			// case WRITE:

			default:
				out0 = Double.NaN;
		}
	} catch (Exception e) {
		out0 = Double.NaN;
	}
	if (2 <= cOut) {
		return new double[] { out0, out1 };
	}
	return new double[] { out0 };
}

/** Handle scalars and simple cases. */
public QVal[] calc( QVal[] arguments ) {
	if ((null == arguments) || (cArgs != arguments.length)) {
		return null;
	}
	final QResult pooled = QResult.get8Pool();
	switch (this) {
		case MINUS:
		case PLUS:
		case SPLIT:
		case TIMES:
			if (QMapSto.isNumeric( arguments[ 0 ] ) && QMapSto.isNumeric( arguments[ 1 ] )) {
				switch (this) {
					case MINUS:
						return SUB.calc( arguments );
					case PLUS:
						return ADD.calc( arguments );
					case SPLIT:
						return DIV.calc( arguments );
					case TIMES:
						return MUL.calc( arguments );
					default:
						;
				}
			}
		default:
			;
	}
	QVal out0 = null;
	QVal out1 = null;
	int cOut = 1;
	boolean todo = false;
	String str = "";
	double num;
	int inx = 0;
//	byte[] dat;
//	long[] handles;
	try {
		switch (this) {
			case ALL:
				todo = true;
				break;
			case AT:
				todo = true;
				break;
			case CONC:
				todo = true;
				break;
			case CUT:
				todo = true;
				break;
			case DATI:
			case DATMS:
			case DATSL:
				num = QMapSto.double4QVal( arguments[ 0 ] );
				if (!Double.isNaN( num )) {
					final long vx = (long) num;
					final long xmint = vx * 100 % 100;
					final long xsec = vx * 10000 % 100;
					switch (this) {
						case DATI:
							str = String.format( "%d-%02d-%02dT%02d:%02d", (int) vx / 10000, ((int) vx / 100) % 100,
								(int) vx % 100, xmint, xsec );
							break;
						case DATMS:
							str = MiscFunc.dateLocal4Millis( true, vx );
							break;
						case DATSL:
							str = MiscFunc.date4SlotSecond( num );
							break;
						default:
							;
					}
					out0 = QMapSto.qval4AtomicValue( pooled, str );
				} else {
					out0 = QMapSto.NaN;
				}
				break;
			case MDROP:
				todo = true;
				break;
			case DUP:
				out0 = arguments[ 0 ];
				out1 = out0;
				cOut = 2;
				break;
			case EDUPTO:
				todo = true;
				break;
			case ELEMENT:
				todo = true;
				break;
			case EMPTY:
				todo = true;
				break;
			case EQ:
				todo = true;
				break;
			case EXISTS:
				todo = true;
				break;
			case EXPAND:
				todo = true;
				break;
			case FILTER:
				todo = true;
				break;
			case GE:
				todo = true;
				break;
			case GT:
				todo = true;
				break;
			case INTERSECTION:
				todo = true;
				break;
			case IS:
				todo = true;
				break;
			case IDAT: {
				String date = QMapSto.string4QVal( arguments[ 0 ] ).replaceAll( "[^0-9T]", "" ).replace( 'T', '.' );
//				long v0 = MiscFunc.leapMinute256ForDate( date );..
				out0 = QMapSto.qval4AtomicValue( pooled, date );
			}
				break;
			case IN:
				todo = true;
				break;
			case ISNT:
				todo = true;
				break;
			case LE:
				todo = true;
				break;
			case LT:
				todo = true;
				break;
			case EMAP:
				todo = true;
				break;
			case MSEC:
				todo = true;
				break;
			case NE:
				todo = true;
				break;
			case NOP:
				todo = false;
				break;
			case OF:
				todo = true;
				break;
			case ONEOF:
				todo = true;
				break;
			case OPT:
				todo = true;
				break;
			case PLUS:
				str = QMapSto.string4QVal( arguments[ 0 ] ) + QMapSto.string4QVal( arguments[ 1 ] );
				out0 = QMapSto.qval4AtomicLiteral( pooled, str );
				break;
			case TIMES:
				str = QMapSto.string4QVal( arguments[ 0 ] );
				double factor = QMapSto.double4QVal( arguments[ 1 ] );
				if ((2 <= factor) && (factor <= 1000)) {
					int i0 = (int) factor;
					StringBuilder out = new StringBuilder( i0 * str.length() + 2 );
					for (; i0 > 0; -- i0) {
						out.append( str );
					}
					str = out.toString();
					out0 = QMapSto.qval4AtomicLiteral( pooled, str );
				}
				break;
			case ZRXG:
				out0 = QMapSto.NaN;
				try {
					inx = (int) QMapSto.double4QVal( arguments[ 2 ] ); //Integer.parseInt( QMap.literal4QVal(arguments[ 2 ].toString() );
					final Pattern pat = Pattern.compile( QMapSto.string4QVal( arguments[ 1 ] ) );
					final Matcher mat = pat.matcher( QMapSto.string4QVal( arguments[ 0 ] ) );
					if (mat.find()) {
						out0 = QMapSto.qval4String( pooled, mat.group( inx ) );
					}
				} catch (Exception e) {
				}
				break;
			case ZRXN:
			case ZRXT:
				try {
					inx = (int) QMapSto.double4QVal( arguments[ 2 ] ); //Integer.parseInt( arguments[ 2 ]) );
				} catch (Exception e) {
					out0 = QMapSto.NaN;
					break;
				}
				// Fall through.
			case ZRX: {
				int count = (0 <= inx) ? inx : 99999;
				int i0 = -1;
				try {
					final Pattern pat = Pattern.compile( QMapSto.string4QVal( arguments[ 1 ] ) );
					final Matcher mat = pat.matcher( QMapSto.string4QVal( arguments[ 0 ] ) );
					while (mat.find()) {
						++ i0;
						if (i0 >= count) {
							break;
						}
					}
					if (0 > inx) {
						count = i0 + inx + 1;
						i0 = count - 1;
						if (0 <= count) {
							mat.reset();
							i0 = -1;
							while (mat.find()) {
								++ i0;
								if (i0 >= count) {
									break;
								}
							}
						}
					}
					if (i0 < count) {
						out0 = QMapSto.NaN;
					} else if (this == ZRXT) {
						out0 = QMapSto.qval4String( pooled, mat.group() );
					} else {
						out0 = QMapSto.qval4Double( mat.start() );
					}
				} catch (Exception e) {
					out0 = QMapSto.NaN;
				}
			}
				break;
			case RLDOWN:
				todo = true;
				break;
			case RLUP:
				todo = true;
				break;
			case ROT:
				todo = true;
				break;
			case SELECT:
				todo = true;
				break;
			case SIZE:
				todo = true;
				break;
			case SMALL:
				todo = true;
				break;
			case SOME:
				todo = true;
				break;
			case SORT:
				todo = true;
				break;
			case SPLIT:
				todo = true;
				break;
			case SUBSET:
				todo = true;
				break;
			case SUPERSET:
				todo = true;
				break;
			case SWAP:
				todo = true;
				break;
			case TAIL:
				todo = true;
				break;
			case TAKE:
				todo = true;
				break;
			case UNION:
				todo = true;
				break;
			default: {
				double[] doubles = new double[ arguments.length ];
				int cnt = 0;
				for (QVal arg : arguments) {
					doubles[ cnt ++ ] = QMapSto.double4QVal( arg );
				}
				doubles = calc( doubles );
				cOut = doubles.length;
				if (2 <= cOut) {
					out1 = QMapSto.qval4Double( doubles[ 1 ] );
				}
				if (1 <= cOut) {
					out0 = QMapSto.qval4Double( doubles[ 0 ] );
				}
			}
		}
		if (todo) {
			out0 = QMapSto.qval4AtomicLiteral( pooled, "(function not implemented)" );
		}
	} catch (Exception e) {
		return null;
	}
	if (2 <= cOut) {
		return new QVal[] { out0, out1 };
	} else if (null == out0) {
		return null;
	}
	return new QVal[] { out0 };
}
//=====
}
