// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

/** For multiple return values with up to 255 threads. */
public final class QResult {
//=====

private static final int LIMIT = 256;
public static final byte NULL = 0;

public long long0;
public long long1;
public long long2;
public long long3;
public double double0;
public double double1;
public double double2;
public Object object0;
public Object object1;
public Object object2;
public Object o4Array;

private static QResult[] mPerThread = new QResult[ LIMIT ];
private static Thread[] mThreads = new Thread[ LIMIT ];

public static QResult get8Pool( byte xiThread ) {
	if (0 == xiThread) {
		return null;
	}
	final int inx = xiThread & 0xff;
	if (null == mThreads[ inx ]) {
		mThreads[ xiThread ] = Thread.currentThread();
		// Object is/ was 'recycled':
		mPerThread[ inx ] = (null != mPerThread[ inx ]) ? mPerThread[ inx ] : new QResult();
	} // else assert threads[inx] == Thread.currentThread();
	return mPerThread[ inx ];
}

private static synchronized byte getThreadIndexSynchronized( Thread me ) {
	for (int i0 = 1; i0 < mThreads.length; ++ i0) {
		if (null == mThreads[ i0 ]) {
			mThreads[ i0 ] = me;
			// Object is/ was 'recycled'?
			mPerThread[ i0 ] = (null != mPerThread[ i0 ]) ? mPerThread[ i0 ] : new QResult();
			return (byte) i0;
		}
	}
	return (byte) 0;
}

public static byte getThreadIndex() {
	Thread me = Thread.currentThread();
	for (int i0 = 1; i0 < mThreads.length; ++ i0) {
		if (me == mThreads[ i0 ]) {
			return (byte) i0;
		}
	}
	return getThreadIndexSynchronized( me );
}

public static QResult get8Pool() {
	return get8Pool( getThreadIndex() );
}

public static void drop8Pool() {
	byte me = getThreadIndex();
	if (0 != me) {
		mPerThread[ me & 0xff ].recycle();
	}
	mThreads[ me & 0xff ] = null;
}

public void recycle() {
	long0 = 0;
	long1 = 0;
	long2 = 0;
	double0 = 0.0;
	double1 = 0.0;
	double2 = 0.0;
	object0 = null;
	object1 = null;
	object2 = null;
	o4Array = null;
}

//=====
}
