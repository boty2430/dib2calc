// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_any.QMapSto.QVal;

public final class CommandBaton extends QBaton {
//=====

public QCalc operator;
//public String uiCommand;
public char uiKeyOrButton;
public String uiParameter;
public QVal[][] vals;
//public String error;

//=====
}