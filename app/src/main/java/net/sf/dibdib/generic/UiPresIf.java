// Copyright (C) 2020  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

/** Passive or active presentation gateway, for UI thread.
 */
public interface UiPresIf extends QDspPrcsIf {
//=====

//void init( QDispatcher xmGateIn, QDispatcher xmGateOut );

QPlace getQPlace();

boolean start();

boolean stop( boolean fast );

/** Handle touch or click event. 
 * @param xXReal X in window.
 * @param xYReal Y in window.
 * @param xXSubOffset X window offset.
 * @param xYSubOffset Y window offset.
 * @param xbClickclickLong TODO ... b0 = confirmed click (for selections), b1 = long click.
 * @return Matching key value or 0.
 */
char touch( int xXReal, int xYReal, int xXSubOffset, int xYSubOffset, long xbClickclickLong );

//QPlace peek();

//=====
}
