// Copyright (C) 2016,2020  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

/** (Q)Transposer interface: Combining Petri Place (QPlace) + Transition:
 * -- QDispatcher (single Place with typed/guarded Transitions)
 * -- QProcess (Places with single Transition).
 */
public interface QDspPrcsIf {
//=====

//Token-/Thread-Oriented Programming style (TOP), based on Petri nets.
//
//IDEA
//
//Petri Arcs are implemented as simple pipes with a buffering end-point and a referencing start-point.
//Petri Places and Transitions are 'normalized' by specific element groups: Transposers and Stores.
//Transposers = active elements: Process, Dispatcher/ trigger, Terminator (sink/ source).
//Places = buffered ports preceding the transitions, for the incoming data tokens = batons.
//Transposers 'process' the data and pass the results on as in a relay race (contrary to callable
//functions/ methods). They use the Batons like arguments and the data in the Stores like parameters.
//Outgoing ports (= results) are implemented as a simple references to the succeeding in-port buffers.
//==> No queue or extra synchronization for Places: one-to-one connections avoid internal race conditions.
//==> Several such (Q)Nets can be connected via dedicated Places: Sinks and Sources.
//
//MODEL
//
//Place's buffer 'x' (in-port): buffering endpoint of Petri Arc for Petri Place 'O':
//-- Process fires if all in-ports/ places have data.
//-- Dispatcher collects data (unguarded) and passes it on according to Transition's guards.
//-- Store is implemented as container with synchronized methods.
//-- Sinks and sources are implemented as Dispatchers with dedicated/ exposed QPlaces.
//
//Process;               Dispatcher:            Store ('synchronized', feeding data tokens)
//---x O -\             ---x\      /-|--->          <----.
//.        \|--->            \    /            --x  v   /|--->
//---x O ---|           ----x O <              ---x O -<
//.        /|--->            /    \            --x  ^   \|--->
//---x O -/             ---x/      \-|--->          <----'
//
//Error handling: asst() for assertions logs ERROR line:
//==> Use error values and options for flushing the (Q)Places.
//
//IMPLEMENTATION
//
//(Q)Batons (data tokens) do not carry the 'full load' of data, because the data gets stored in
//stateful data containers: (Q)Stores.
//-- (Q)Stores are stateful (e.g. backed by files),
//-- (Q)Transposers are stateless.
//-- Such Petri nets (QNets) get connected to the outside world via dedicated QPlaces.
//Batons and the Container entries have a timestamp, i.e. containers keep latest versions
//or version history.
//Consistency of the data is possible using the timestamps: only if all tokens with a certain
//timestamp have entered a sink and no token with a lower timestamp is left,
//then the (Q)Net (context) data with that timestamp is considered done.
//If necessary, a rollback to that state of the QNet data is possible.
//Due to parallel processing of tokens the timestamped data values may have 'glitches'
//=> Transposers and Stores use the timestamps to minimize non-deterministic values.
//Depending on the usage and the target platform, several or all QProcesses in a QNet
//may run on a single thread (via scheduler), or have their own thread. If, for performance
//reasons, extra data needs to be shared between processes/ threads, then Java's package
//visibility may be utilized: all processes/ classes in a package run on the same thread
//and do not need to synchronize their data usage with restricted access.
;

/** Initialize and connect to other transposers via their QPlaces.
 * @param xrOutgoing References to the other's incoming places.
 * @return true iff successful.
 */
public boolean init( QPlace... xrOutgoing );

/** Peek into implementer's QPlaces.
 * @return null if empty, unprocessed data otherwise.
 */
public QBaton peek( long... xbOptFlags );

/** Perform small processing step. If processing is finished, drop outgoing QBatons in
 * receiver's Place(s) and return main one.
 * @return (main) QBaton if finished, PENDING if still processing,
 * null if terminated/ nothing changed.
 */
//public QBaton step();

//=====
}
