// Copyright (C) 2016,2017,2018 Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

import java.util.Arrays;
import net.sf.dibdib.config.Dib2Constants;
import net.sf.dibdib.thread_any.MiscFunc;

/** Transitional ...
 * Element (record) of categorical database (currently saved as CSV line), also used as Token:
 * name (atomic) + propositional main category (+ secondary categories)
 * + time stamp + source of information
 * + (propositional) data.
 * (Constraint logic, name as partial ID)
 * Prolog interpretation: (CATEGORY ATOM), (CATEGORY.DATA_LABEL ATOM DATA_ENTRY).
 */
public class Mapping_OLD { // extends OidBaton {
//=====

//public static final String version = 0.5 // "0.2";
public static final String fieldNames = //,
	"" // OID
		+ "\tNAME" //,
		+ "\tCATS" //,
		+ "\tTIME" //,
		+ "\tCONTRIB" //,
		+ "\tSRCS" //,
		+ "\tRECV" //,
		+ "\tDAT1" //,
		+ "\tDAT2" //,
		+ "\tDAT3" //,
		+ "\tDAT4" //,
		+ "\tDAT5" //,
		+ "\tDAT6" //,
		+ "\tDAT7" //,
		+ "\tDAT8" //,
		+ "\tDAT9";

public static String qRootUser = "0@0";
public static String qRootOid = MiscFunc.createId( qRootUser );
//public static QMapping qRoot = new QMapping( QStruct.makeHandle( "ROOT" ), -1, QMapping.Cats.VAR.handle, QStruct.NONE,
//	QStruct.makeHandle( qUser ) );
//public static final long khsNN = QMap.makeHandle( "N.N." );

public long timeStamp;

///// -- Immutable - 'as if' (whenever not enforced by Java)

public String oid;

/** Name of (quasi-atomic) element, e.g. 'Charlie Brown' or 'Brown.Charlie' */
public final String label;

public final String[] categories;

/** Flags for pre-defined categories */
//public final long bCategories;

public final String contributorOid;
//public final String[] sources;
//public final String[] shareReceiverOids;
public final String[] listElements;
public final long ack; // time/ -1: not sent, 0: done

//public final String[] listShashes;

/** Label (without '*') + timeStamp/-1 + OID/null + Cats + contributor/0 + values. */
//public Mapping( String xLabel, long xTimeStamp, String xOldObjectId, String xCategories, long xhContributor, String... xmValues ) {
public Mapping_OLD( String xmLabel, long xmTimeStamp, String objectId, String xmCategories, String xmContributorOid, long ack,
	String... xmValues ) {
//	super( objectId, xmTimeStamp, 0 );
	oid = (null != objectId) //,
	? objectId //,
		: MiscFunc.createId( ((1 < xmLabel.length()) || (0 >= xmValues.length) || (1 >= xmValues[ 0 ].length())) ? xmLabel : xmValues[ 0 ] );
	label = xmLabel;
	categories = xmCategories.split( "[^0-~]+" );
	timeStamp = (0 <= xmTimeStamp) ? xmTimeStamp : MiscFunc.currentTimeMillisLinearized();
	contributorOid = (null == xmContributorOid) ? "NN" : xmContributorOid;
	this.ack = ack;
	listElements = xmValues;
}

public Mapping_OLD( String[] a0, int iOid, int flagsMarkAdjust4Time ) {
	long time = MiscFunc.millis4Date( a0[ 3 ] );
	if (0 != (flagsMarkAdjust4Time & 1)) {
		time = time & ~Dib2Constants.TIME_SHIFTED;
	}
	long min = (0 != (flagsMarkAdjust4Time & 2)) ? 1 : time;
	timeStamp = MiscFunc.alignTime( time, min );
	categories = a0[ 2 + iOid ].split( "[^0-~]+" );
	label = a0[ 1 + iOid ];
	oid = (0 <= iOid) ? a0[ iOid ] : MiscFunc.createId( label, timeStamp );
	contributorOid = a0[ 4 + 2 * iOid ];
	ack = ((0 != iOid) || (3 > a0[ 6 ].length())) ? 0L
		: (a0[ 6 ].contains( "X" ) ? -1L
			: MiscFunc.millis4Date( a0[ 6 ].substring( 1 + a0[ 6 ].indexOf( ':' ) ) ));
	listElements = Arrays.copyOfRange( a0, 7 + 3 * iOid, a0.length );
}

@Override
public String toString() {
	return label + '@' + MiscFunc.dateShort4Millis( timeStamp ) + '=' //,
		+ ((0 < listElements.length) ? listElements[ 0 ] : "");
}

public String getContributorOid( String xRootOid ) {
	if ((null == contributorOid) || (0 == contributorOid.length())) {
		return xRootOid;
	}
	return contributorOid;
}

public static void toCsvLine( Mapping_OLD entry, StringBuilder out ) {
	out.append( entry.oid );
	out.append( '\t' ).append( entry.label );
	out.append( '\t' ).append( (0 < entry.categories.length) ? entry.categories[ 0 ] : "" );
	boolean isMsg = false;
	for (int i0 = 1; i0 < entry.categories.length; ++ i0) {
		out.append( ' ' ).append( entry.categories[ i0 ] );
		isMsg = isMsg || "MSG".equals( entry.categories[ i0 ] );
	}
	out.append( '\t' ).append( MiscFunc.date4Millis( false, entry.timeStamp ) );
	out.append( '\t' ).append( entry.contributorOid );
	String rcvAck = "*:" + ((0 > entry.ack) ? 'X' : MiscFunc.date4Millis( true, entry.ack ));
	out.append( "\t\t" + (isMsg ? rcvAck : "") ); // sources, recv simplified
	for (String el : entry.listElements) {
		out.append( '\t' ).append( el.replace( '\n', '\t' ) );
	}
	out.append( '\n' );
}
//=====
}
