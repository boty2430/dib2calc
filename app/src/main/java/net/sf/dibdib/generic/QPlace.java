// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

import java.util.Arrays;

/** For token flow/ relay model (cmp. QDspPrcs), based on Petri net (Arc weight = 1)
 * with typed/ guarded Places for QBatons (data tokens).
 * (Places impose guards on their preceding Transitions).
 * Elements of net ('Context'):
 * - (Q)Process (synchronized split): incoming (Q)Places with single Arcs + single Transition.
 * - (Q)Dispatcher (merged choice): single (unguarded) incoming Place + identity Transitions.
 * - (Q)Store (merged source): single guarded Place + copy Transitions with returning Arc.
 * - Plus (Q)Terminators (source, sink) for workflow: used for gateways/ bridges/ external transitions.
 */
public class QPlace {
//=====

private volatile QBaton[] mBatons = new QBaton[ 4 ];
private volatile int iInBaton = 0;
private volatile int iOutBaton = 0;

public QBaton peek() {
	return ((iInBaton == iOutBaton) && (null == mBatons[ iOutBaton ])) ? null : mBatons[ iOutBaton ];
}

/** For guarded transitions, to be overridden.
 * @param xBaton Instance of (sub-) class.
 * @return true if the Baton is acceptable.
 */
public boolean matches( QBaton xBaton ) {
	return true;
}

/** To be run on sender's thread. */
public int push( QBaton xmBaton ) {
	if ((iInBaton == iOutBaton) && (16 < mBatons.length) && (iInBaton < mBatons.length / 2)) {
		mBatons = Arrays.copyOf( mBatons, mBatons.length / 2 );
	}
	int iIn = (iInBaton + 1) % mBatons.length;
	int old = iInBaton;
	if (iIn == iOutBaton) {
		// Temporarily block the other outgoing data.
		iInBaton = iIn;
		if (iOutBaton <= iInBaton) {
			///// Ok, it is blocked.
			iIn = mBatons.length;
			QBaton[] ox = Arrays.copyOf( mBatons, mBatons.length * 2 );
			ox[ iIn ] = xmBaton;
			mBatons = ox;
			iInBaton = old;
		} else {
			///// The other thread was faster.
			mBatons[ old ] = xmBaton;
		}
	} else {
		// This one first, explicitly using the volatile variable.
		mBatons[ iInBaton ] = xmBaton;
		iInBaton = iIn;
	}
	return old;
}

/** To be run on dispatcher's/ receiver proc's thread. */
public QBaton pull() {
	int iIn = iInBaton;
	QBaton[] old = mBatons;
	if (iIn == iOutBaton) {
		return null;
	}
	if (null == old[ iOutBaton ]) {
		iOutBaton = 0;
	}
	for (; (iOutBaton < iIn) && (null == old[ iOutBaton ]); ++ iOutBaton) {
	}
	if (iOutBaton == iIn) {
		return null;
	}
	QBaton out = old[ iOutBaton ];
	mBatons[ iOutBaton ] = null;
	iOutBaton = (iOutBaton + 1) % mBatons.length;
	return out;
}

/** To be run on dispatcher's/ receiver proc's thread. */
public void flush( boolean resize ) {
	iOutBaton = iInBaton;
	for (int i0 = mBatons.length - 1; i0 >= 0; -- i0) {
		mBatons[ i0 ] = null;
		if (resize && (i0 < iOutBaton)) {
			iOutBaton = i0;
			iInBaton = i0;
		}
	}
}

//=====
}
