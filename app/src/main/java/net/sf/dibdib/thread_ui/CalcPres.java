// Copyright (C) 2018,2019 Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_ui;

import java.io.File;
import java.util.Arrays;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_any.QMapSto.QVal;
import net.sf.dibdib.thread_wk.CcmRunner;

import static net.sf.dibdib.thread_any.QCalc.*;
import static net.sf.dibdib.thread_any.StringFunc.*;
import static net.sf.dibdib.thread_ui.UiDataSto.*;

/** For platform's UI thread, singleton gateway. */
public enum CalcPres implements UiPresIf {
//=====

INSTANCE;

///// Threaded (in/ out/ trigger)

public static volatile boolean wxExitRequest = false;

final QPlace wxGateIn = new QPlace();
private QPlace rGateOut; // TODO Use it for events ...

/////

final AboutViews qAboutVw = new AboutViews();
final CalcVw qCalcVw = new CalcVw();
final CcmListVw qListVw = new CcmListVw();

/////

@Override
public boolean init( QPlace... xrOutgoingGate ) {
	rGateOut = xrOutgoingGate[ 0 ];
	qSwitches = SW_DEFAULT.clone();
//	if (0 >= CsvDb.instance.stackSize() && QMap.main.isFresh() && (null == CsvCodec0.getPassFull())) {
	if (!CcmSto.instance.isInitialized()) {
//		CcmRunner.initLoad( true );
		///// Prepare setup (loading data or intro).
		UiDataSto.pathInitDataFile = CcmRunner.check4Load();
		///// Try with dummy phrase -- maybe good enough for simple calculator etc.
		if (TcvCodec.instance.setDummyPhrase( false )) {
			if (null == UiDataSto.pathInitDataFile) {
				CcmRunner.INSTANCE.addSamples();
				qSwitches[ UiDataSto.SWI_STEP_OR_ACCESS_MINUTE ] = 600;
				// Mark as initialized:
				CcmSto.instance.load( null );
//			} else if (CsvCodec0.checkAccessCode( null )) {
//				qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ] = 1;
//			} else {
//				qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ] = 100;
			}
		} else {
			// Access code required:
			qSwitches[ UiDataSto.SWI_STEP_OR_ACCESS_MINUTE ] = 200;
		}
	} else { // if (1000 <= qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ]) {
		qSwitches[ UiDataSto.SWI_STEP_OR_ACCESS_MINUTE ] = 999;
	}
	return true;
}

@Override
public QPlace getQPlace() {
	return wxGateIn;
}

@Override
public QBaton peek( long... xbOptFlags ) {
	return wxGateIn.peek();
}

private CcmVwIf[] findView_map = null;

public CcmVwIf findView() {
	int swVal = UiDataSto.qSwitches[ UiDataSto.SWI_VIEW ];
	if (1000 > qSwitches[ UiDataSto.SWI_STEP_OR_ACCESS_MINUTE ]) {
		swVal = SW_VAL_VIEW_Intro;
	} else if (wxExitRequest) {
		return qAboutVw.qLicenseVw;
	}
	if (null == findView_map) {
		findView_map = new CcmVwIf[ UiDataSto.SW_VAL_VIEWs ];
		findView_map[ UiDataSto.SW_VAL_VIEW_Help ] = qAboutVw.qCcmHelpVw;
		findView_map[ UiDataSto.SW_VAL_VIEW_Intro ] = qAboutVw.qCcmInitialVw;
		findView_map[ UiDataSto.SW_VAL_VIEW_License ] = qAboutVw.qLicenseVw;
		findView_map[ UiDataSto.SW_VAL_VIEW_List ] = qListVw;
		findView_map[ UiDataSto.SW_VAL_VIEW_Standard ] = qCalcVw;
	}
	return findView_map[ swVal ];
}

public boolean consolidateTextLines() {
//	boolean changed = false;
	int len = qPageLinesFeed.lines.length - 1;
	for (; len >= 0; -- len) {
		if (null == qPageLinesFeed.lines[ len ]) {
			continue;
		}
		if ((len < qPageLinesFeed.count) && (0 < qPageLinesFeed.lines[ len ].trim().length())) {
			break;
		}
		qPageLinesFeed.lines[ len ] = null;
	}
	++ len;
	qPageLinesFeed.count = len;
	if (qPageLinesFeed.lines.length >= (10 + len * 4)) {
		qPageLinesFeed.lines = Arrays.copyOf( qPageLinesFeed.lines, 10 + 2 * len );
	}
	if (qPageLinesFeed.textLineInx >= len) {
		qPageLinesFeed.textLineInx = len;
		qPageLinesFeed.textLineCharInx = (0 > len) ? -1 : 0;
	} else if (qPageLinesFeed.textLineCharInx > qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ].length()) {
		qPageLinesFeed.textLineCharInx = qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ].length();
	}
	if (qPageLinesStable.lines.length < len) {
		qPageLinesStable.lines = new String[ len * 2 ];
	} else if (qPageLinesStable.lines.length >= (10 + len * 4)) {
		qPageLinesStable.lines = Arrays.copyOf( qPageLinesStable.lines, 10 + 2 * len );
	}
	qPageLinesStable.count = qPageLinesFeed.count;
	for (int i0 = qPageLinesStable.lines.length - 1; i0 >= 0; -- i0) {
//		String old = qPageLinesStable.lines[ i0 ];
		qPageLinesStable.lines[ i0 ] = (len > i0) ? qPageLinesFeed.lines[ i0 ] : null;
//		changed = changed || ((null == old) ? (null != qxPageLines.lines[ i0 ])
//			: !old.equals( qPageLinesStable.lines[ i0 ] ));
	}
	qPageLinesStable.setTimeStamp();
	if (qPageLinesStable.textLineInx >= len) {
		qPageLinesStable.textLineInx = len;
		qPageLinesStable.textLineCharInx = (0 > len) ? -1 : 0;
	} else if (qPageLinesStable.textLineCharInx > qPageLinesStable.lines[ qPageLinesStable.textLineInx ].length()) {
		qPageLinesStable.textLineCharInx = qPageLinesStable.lines[ qPageLinesStable.textLineInx ].length();
	}
	return true; // changed;
}

public void prepareTextLines( boolean is4Console ) {
	CcmVwIf vw = findView();
	//Log.d( "prepView", "a " + qSwitches[ Dib2Ui.SWI_ACCESS_MIU_OR_INTRO ] );
	if (1000 > qSwitches[ UiDataSto.SWI_STEP_OR_ACCESS_MINUTE ]) {
		qPageLinesFeed.count = vw.prepareTextLines( false, qBarDialog[ BARI_TEXTFIELD ] );
		qPageLinesFeed.lines = vw.getTextLines(); //((AboutVw.CcmIntroVw) vw).qLines;
		if (990 < qSwitches[ UiDataSto.SWI_STEP_OR_ACCESS_MINUTE ]) {
			switchView8Init( qSwitches, qBarTitle, qBarTool );
			vw = findView();
			qPageLinesFeed.lines[ 0 ] = qBarDialog[ BARI_TEXTFIELD ];
			qBarDialog[ BARI_TEXTFIELD ] = "";
			qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] = 0;
		} else {
			return;
		}
	}
	qTemplates = CcmTemplates.Dib2UiP_Templates_Default(); // ...
	final String txt = (null == qBarDialog[ BARI_TEXTFIELD ]) ? null
		: StringFunc.makePrintable( StringFunc.string4Mnemonics( qBarDialog[ BARI_TEXTFIELD ] ) );
	int len = vw.prepareTextLines( is4Console, txt );
	qPageLinesFeed.lines = vw.getTextLines();
	qPageLinesFeed.count = len;
	if (null == qPageLinesFeed.lines) {
		qPageLinesFeed.lines[ 0 ] = "";
		if (0 < qBarDialog[ BARI_TEXTFIELD ].length()) {
			qPageLinesFeed.lines[ 0 ] = "   " + qBarDialog[ BARI_TEXTFIELD ].substring( 0, qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] )
				+ '|' + qBarDialog[ BARI_TEXTFIELD ].substring( qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] );
		}
	}
}

private long handleEventInitial_lastAttempt = 0;

/**
 * @return 1 if done, 0 if partial, -1 if n/a
 */
private int handleEventIntro( String command, char key ) {
	if (1000 <= qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ]) {
		return -1;
	}
	final int curMin = (int) (MiscFunc.currentTimeMillisLinearized() / 1000 / 60);
	if (command.equals( UiDataSto.SW_NAME_LANGUAGE__LANG )) {
		qSwitches[ SWI_LANGUAGE ] = (qSwitches[ SWI_LANGUAGE ] + 1) % Dib2Local.kLanguages.length;
		return 1;
	} else if (610 > qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ]) {
		qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ] = 601;
	} else if (('0' <= key) && (key <= '9')) {
		qBarDialog[ BARI_TEXTFIELD ] += "" + key;
		return 1;
	}
	qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ] = qAboutVw.qCcmInitialVw.getNextStep( qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ] );
	switch (key) {
		case '+':
		case '*':
		case '\u00b1':
			if (0 < qBarDialog[ BARI_TEXTFIELD ].length()) {
				final QResult pooled = QResult.get8Pool();
				CcmSto.instance.stackPush( QMapSto.qvalAtoms4String( pooled, qBarDialog[ BARI_TEXTFIELD ] ) );
				qBarDialog[ BARI_TEXTFIELD ] = "";
			}
			QCalc op = QCalc.getOperator( "" + key );
			if (null == op) {
				return 0;
			}
			int cArgs = op.cArgs;
			QVal[][] args = CcmSto.instance.stackPop( cArgs, op );
			if (null != args) {
				QVal[] args0 = QMapSto.pickItems( 0, args );
				final QVal[] vals = op.calc( args0 );
				CcmRunner.INSTANCE.pushResult( new QVal[][] { vals }, -1 );
			}
			break;
		case CR:
			if (0 < qBarDialog[ BARI_TEXTFIELD ].length()) {
				final QResult pooled = QResult.get8Pool();
				CcmSto.instance.stackPush( QMapSto.qvalAtoms4String( pooled, qBarDialog[ BARI_TEXTFIELD ] ) );
				qBarDialog[ BARI_TEXTFIELD ] = "";
			}
			break;
		case SEND:
			break;
		case ESCAPE:
			CcmSto.instance.stackClear( false );
			if (600 <= qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ]) {
				qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ] = (990 > qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ]) ? 990 : curMin;
				CcmSto.instance.load( null );
			}
			break;
		default:
			return 1;
	}
	if (990 < qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ]) {
		qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ] = curMin;
		CcmSto.instance.load( null );
	}
	return 1; // (100 >= qSwitches[ SWI_INTRO_STEP ]);
}

/**
 * @return 1 if done, 0 if partial, -1 if n/a
 */
private int handleEventInitial( String command, char key ) {
	final int accessStatus = qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ];
	final byte[] pass = TcvCodec.instance.getPassFull();
	final boolean proper = (null != pass) && TcvCodec.instance.setHexPhrase( "" ); // (4 < pass.length);
	final boolean intro = (500 <= accessStatus); //(null != pass) && !proper;
	if (CcmSto.instance.isInitialized()) {
		if (intro) {
			return handleEventIntro( command, key );
		}
//		if (900 <= qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ]) {
//			return -1;
//		}
		if (200 <= qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ]) {
			return -1;
		}
		// Checking AC in the following steps.
	}
	if ((null != command) && (0 < command.length())) {
		return 0;
	}
	final int curMin = (int) (MiscFunc.currentTimeMillisLinearized() / 1000 / 60);
	if ((300 < accessStatus) && (0 < handleEventInitial_lastAttempt)) {
		if ((ESCAPE == key) && "0".equals( qBarDialog[ BARI_TEXTFIELD ] )) {
			// Give up on loading, as fallback.
			qSwitches[ UiDataSto.SWI_STEP_OR_ACCESS_MINUTE ] = curMin;
			CcmSto.instance.load( null );
			return 0;
		}
	}
	if ((SEND != key) && (DONE != key) && (CR != key)) {
		return -1;
	}
	int next4Fail = accessStatus + ((90 > (accessStatus % 100)) ? 5 : 0);

	///// Enforce some delay.
	int deltaMsec = 1 * 1000;
//	if (350 < accessStatus) {
//		deltaMsec = 10 * 3600 * 1000;
//	} else 
	if (50 <= (accessStatus % 100)) {
		deltaMsec = 60 * 1000;
	}
	if (handleEventInitial_lastAttempt + deltaMsec > MiscFunc.currentTimeMillisLinearized()) {
		return 1;
	}

	handleEventInitial_lastAttempt = MiscFunc.currentTimeMillisLinearized();
	if (200 > accessStatus) {
		if (CcmSto.instance.isInitialized()) {
			qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ] = next4Fail;
			if (proper //&& CsvDb.instance.checkAccessCode( null )) {
				&& (130 < next4Fail)) {
				///// Retry count kicks in.
				File filePw = new File( Dib2Config.platform.getFilesDir( "safe" ), Dib2Config.PASS_FILENAME );
				if (filePw.exists()) {
					filePw.delete();
				}
				return 1; // Until user exits ...
			} else if (TcvCodec.instance.checkAccessCode( StringFunc.bytesUtf8( qBarDialog[ BARI_TEXTFIELD ] ) )) {
				qSwitches[ UiDataSto.SWI_STEP_OR_ACCESS_MINUTE ] = curMin;
			}
		} else {
//			if (null != UiSwitches.pathInitDataFile) {
			qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ] = 200;
		}
		return 0;//1
	} else if (300 > accessStatus) {
		TcvCodec.instance.setAccessCode( StringFunc.bytesUtf8( qBarDialog[ BARI_TEXTFIELD ] ) );
		next4Fail += 100;
		File filePw = new File( Dib2Config.platform.getFilesDir( "safe" ), Dib2Config.PASS_FILENAME );
		if (!filePw.exists()) {
		} else if (230 <= accessStatus) {
			filePw.delete();
		} else {
			TcvCodec.instance.setHexPhrase( null );
		}
	} else {
		next4Fail -= 100;
		TcvCodec.instance.setHexPhrase( StringFunc.hex4Bytes( StringFunc.bytesUtf8( qBarDialog[ BARI_TEXTFIELD ] ), false ) );
	}
	qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] = 0;
	qBarDialog[ BARI_TEXTFIELD ] = "";
	qSwitches[ UiDataSto.SWI_STEP_OR_ACCESS_MINUTE ] = next4Fail;
	return 0;
}

private void handleEventDialogText( int xiField, int xiText ) {
	if (0 > handleEventInitial( "", (char) 0 )) {
//		qSwitches[ SWI_KEYPAD ] &= ~0x4;
		setBar4Layers(); //switchLayers( SW_LAYER_KEYPAD );
	}
}

private void handleEventCanvas( int xX, int xY ) {
	if (0 <= handleEventInitial( "", (char) 0 )) {
		return;
	}
//	wqRefreshNeeded = true;
	setBar4Layers(); //switchLayers( qSwitches, qBarTitle, qBarTool, SW_LAYER_CANVAS );
	UiDataSto.qWinTouchedX3PixR[ 2 ] = UiDataSto.qWinTouchedX3PixR[ 1 ];
	UiDataSto.qWinTouchedX3PixR[ 1 ] = UiDataSto.qWinTouchedX3PixR[ 0 ];
	UiDataSto.qWinTouchedX3PixR[ 0 ] = xX;
	UiDataSto.qWinTouchedY3PixR[ 2 ] = UiDataSto.qWinTouchedY3PixR[ 1 ];
	UiDataSto.qWinTouchedY3PixR[ 1 ] = UiDataSto.qWinTouchedY3PixR[ 0 ];
	UiDataSto.qWinTouchedY3PixR[ 0 ] = xY;
	qPageLinesFeed.textLineInx = Dib2Config.ui.getTextRow( xY, qPageLinesFeed.count );
	qPageLinesFeed.textLineCharInx = Dib2Config.ui.getTextColumn( UiDataSto.qWinTouchedX3PixR[ 0 ],
		qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ] );
}

private CommandBaton evaluateEventKey( char key, long xbClickclickLong ) {

	CommandBaton commandData = new CommandBaton();
	// Request refresh at the end:
	commandData.operator = QCalc.DUMP;
	commandData.uiKeyOrButton = key;
	int done = handleEventInitial( "", key );
	if (0 <= done) {
		return (0 < done) ? null : commandData;
	}
	boolean calcMode = (1 == qSwitches[ UiDataSto.SWI_KEYPAD_INDEX ]);
	if ((UiDataSto.qPadKeys.length - 1) <= qSwitches[ UiDataSto.SWI_KEYPAD_INDEX ]) {
		qSwitches[ UiDataSto.SWI_KEYPAD_INDEX ] = UiDataSto.keys_UniBlock_FromPad;
	}
	commandData.operator = QCalc.NOP;
	String cmd = "" + key;
	if ((CR == key) && (UiDataSto.SW_LAYER_CANVAS == (0xf & qSwitches[ UiDataSto.SWI_LAYERS ]))) {
		qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] = 0;
		qBarDialog[ BARI_TEXTFIELD ] = "";
		if (0 <= qPageLinesFeed.textLineCharInx) {
			qPageLinesFeed.lines[ qPageLinesFeed.textLineInx + 1 ] = qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ]
				.substring( qPageLinesFeed.textLineCharInx )
				+ qPageLinesFeed.lines[ qPageLinesFeed.textLineInx + 1 ];
			qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ] = qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ].substring( 0,
				qPageLinesFeed.textLineCharInx );
			qPageLinesFeed.textLineCharInx = 0;
			++ qPageLinesFeed.textLineInx;
			Dib2Config.ui.setTouched4TextPos();
		}
		return commandData;
	} else if (XPASTE == key) {
		cmd = Dib2Config.ui.getClipboard();
		cmd = (null == cmd) ? "" : cmd;
		if ((0 >= qBarDialog[ BARI_TEXTFIELD ].length()) && (0 < cmd.length())) {
//			commandData.operator = QCalc.NOP;
			commandData.uiParameter = cmd;
			return commandData;
		}
	} else if (' ' > key) {
		String txt = qBarDialog[ BARI_TEXTFIELD ];
		if ((null == txt) || (CR != key)) {
			return null;
		} else if ((UiDataSto.SW_VAL_VIEW_Standard == qSwitches[ UiDataSto.SWI_VIEW ])
			&& (1 == qSwitches[ UiDataSto.SWI_KEYPAD_INDEX ])
			&& (1 < txt.length()) && (' ' == txt.charAt( 0 ))) {
			// Allow entry of symbols as text via leading ' '.
			qBarDialog[ BARI_TEXTFIELD ] = txt.substring( 1 );
			return null;
		} else if (txt.startsWith( "{" ) && !txt.endsWith( "}" )) {
			// Unfinished quote.
			cmd = "\t";
		} else if (txt.startsWith( "[" ) && !txt.endsWith( "]" )) {
			// Unfinished list.
			cmd = "\t";
		} else {
			return null;
		}
	}
	if ((UiDataSto.SW_VAL_VIEW_Standard == qSwitches[ UiDataSto.SWI_VIEW ])
		&& calcMode && (0 >= qBarDialog[ BARI_TEXTFIELD ].length())) {
		///// Entry of symbols: as operators.
		if (('A' <= key) && (key <= 'F')) {
			///// For calculator.
			switch (key) {
				case 'A':
					cmd = "XOR";
					break;
				case 'B':
					cmd = "SQRT";
					break;
				case 'C':
					cmd = "CLEAR";
					break;
				case 'D':
					cmd = "RADD";
					break;
				case 'E':
					cmd = "E";
					break;
				case 'F':
					cmd = "PI";
					break;
				default:
					;
			}
			key = 0x80;
		}
		if ((0 <= "+-*/%_~!&|^<>=".indexOf( key )) || (0x80 <= key)) {
			commandData.operator = QCalc.getOperator( (0 <= "<>".indexOf( key )) ? ("" + key + key) : cmd );
			if ((null != commandData.operator) && (NOP != commandData.operator)) {
				final QResult pooled = QResult.get8Pool();
				CcmSto.instance.variable_force( "T", new QVal[] { QMapSto.qval4AtomicLiteral( pooled, commandData.operator.name() ) } );
				commandData.uiParameter = qBarDialog[ BARI_TEXTFIELD ];
				qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] = 0;
				qBarDialog[ BARI_TEXTFIELD ] = "";
				return commandData;
			}
		}
		cmd = ('#' == key) ? "0x" : cmd;
	} else if (0 != (2 & xbClickclickLong)) {
		cmd += "" + key + key + key + key;
		cmd += cmd;
	}
	qBarDialog[ BARI_TEXTFIELD ] = qBarDialog[ BARI_TEXTFIELD ].substring( 0, qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] )
		+ cmd
		+ qBarDialog[ BARI_TEXTFIELD ].substring( qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] );
	qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] += cmd.length();
	if ((UiDataSto.SW_LAYER_CANVAS == (0xf & qSwitches[ UiDataSto.SWI_LAYERS ])) && (0 <= qPageLinesFeed.textLineCharInx)) {
		qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ] = qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ].substring( 0,
			qPageLinesFeed.textLineCharInx )
			+ cmd
			+ qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ].substring( qPageLinesFeed.textLineCharInx );
		qPageLinesFeed.textLineCharInx += cmd.length();
		Dib2Config.ui.setTouched4TextPos();
	}
	return commandData;
}

private CommandBaton evaluateEventTools( String tool, char key, long xbClickclickLong ) {
	int done = handleEventInitial( tool, key );
	CommandBaton commandData = new CommandBaton();
	commandData.operator = QCalc.NOP;
	if (0 <= done) {
		return (0 < done) ? null : commandData;
	}
//	qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] = 0;
//	qBarDialog[ BARI_TEXTFIELD ] = "";
	if ((null == tool) || (0 >= tool.length())) {
		commandData.operator = QCalc.getOperator( "" + key );
		if (' ' > key) {
			return evaluateEventKey( key, xbClickclickLong );
		} else {
			switch (key) {
				case '=':
					return null; // menu
				case 'X':
				case 'A': {
					switchBackOrCircle( xbClickclickLong, SWI_LAYERS, true );
					commandData.operator = QCalc.NOP;
//					setBar4Layers(); //switchLayers( qSwitches, qBarTitle, qBarTool );
					break;
				}
				default:
					return null;
			}
		}
		return commandData;
	}
	commandData.uiParameter = qBarDialog[ BARI_TEXTFIELD ];
	qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] = 0;
	qBarDialog[ BARI_TEXTFIELD ] = "";
	commandData.operator = QCalc.NOP;
	key = 0;
//	boolean doDraw = true;
	if (tool.startsWith( "LA" )) {
		if (0 != (2 & xbClickclickLong)) {
			// TODO
			// switch keyboard
		} // else ...
		switchBackOrCircle( xbClickclickLong, SWI_LANGUAGE, true );
		setToolBar( qBarTool, "LA" + Dib2Local.kLanguages[ qSwitches[ SWI_LANGUAGE ] ], 0 );
//		qDoDraw = true;
//		return commandData;
	} else if (tool.startsWith( "LY" )) {
		final int level = 0xf & tool.charAt( 2 );
//		shiftLayer( qSwitches, qBarTitle, qBarTool, level );
		switchLayers( level == 1 );
//		qBarTitle[ 2 * 4 ] = "";
	} else if (tool.startsWith( "VW" )) {
		switchBackOrCircle( xbClickclickLong, SWI_VIEW, true );
		setToolBar( qBarTool, UiDataSto.SW_NAMES_VIEW[ qSwitches[ SWI_VIEW ] ], 0 );
		qPageLinesFeed.posHome();
		UiDataSto.qPYLineOffs = 0;
	} else {
//		doDraw = false;
		commandData.operator = QCalc.getOperator( tool.trim() );
	}
//	if (doDraw) {
//		wqRefreshNeeded = true;
//		Dib2Config.qPlatform.invalidate();
	return commandData;
}

public CommandBaton handleUiEvent( CommandBaton xyCmd ) {
	QCalc op = xyCmd.operator;
	String par = xyCmd.uiParameter;
	if (null == op) {
		return null;
	}
	int pop = 0;
	QVal[] args;
	if ((null == par) || (0 >= par.length())) {
		xyCmd.uiParameter = null;
		args = CcmSto.instance.stackPeek( 0 );
		pop = 1;
		if (null != args) {
			par = QMapSto.string4QVals( args );
		}
	}
	if (op == EXEC) {
		if ((null == par) || (0 >= par.length())) {
			return null;
		}
//		xyCmd.uiParameter = null;
		// TODO: for [...] take whole stack
		op = QCalc.getOperator( par );
		par = null;
//		xyCmd.operator = op;
		args = CcmSto.instance.stackPeek( pop );
		pop = 1;
		if (null != args) {
			par = QMapSto.string4QVals( args );
		}
	} else {
		pop = 0;
	}

	///// UI related functions first - concurrent setting of switches ...!
	switch (op) {
		case ABOUT:
			UiDataSto.qSwitches[ UiDataSto.SWI_VIEW ] = UiDataSto.SW_VAL_VIEW_License;
			return null;
		case EXIT:
			CalcPres.wxExitRequest = true; //...
//			feedProgress( new String[] { "Program is about to stop ... (Please press ENTER)." } );
			Thread.yield();
			return null;
		case HELP:
			UiDataSto.qSwitches[ UiDataSto.SWI_VIEW ] = UiDataSto.SW_VAL_VIEW_Help;
			return null;
		case PW:
		case PWAC:
			if ((null == par) || (0 >= par.length())) {
				return null;
			}
			if (PW == op) {
				TcvCodec.instance.setHexPhrase( StringFunc.hexUtf8( par, false ) );
			} else {
				TcvCodec.instance.setAccessCode( par.getBytes( StringFunc.STRX16U8 ) );
			}
			// Do not change stack from this thread ...
			op = (1 <= pop) ? CLR2 : CLR1;
			break;
		case QFILTER: {
			int flags = UiDataSto.qSwitches[ UiDataSto.SWI_MAPPING_CATS ] << 1;
			if (Mapping.Cats.OTHERS.flag >= flags) {
				flags = (int) Mapping.Cats.OTHERS.flag;
			} else if (Mapping.Cats.OTHERS.flag == (flags >>> 1)) {
				flags = 1 << 16;
			} else if (Mapping.Cats._MAX.flag == flags) {
				flags = (1 << 31) - 1;
			} else if (Mapping.Cats._MAX.flag < flags) {
				flags = (int) Mapping.Cats.OTHERS.flag;
			}
			UiDataSto.qSwitches[ UiDataSto.SWI_MAPPING_CATS ] = flags;
			UiDataSto.qSwitches[ UiDataSto.SWI_VIEW ] = UiDataSto.SW_VAL_VIEW_List;
			setToolBar( qBarTool, UiDataSto.SW_NAMES_VIEW[ qSwitches[ SWI_VIEW ] ], 0 );
		}
			return null;
//			op = NOP;
//			break;
		case UICOD:
			if ((null == par) || (0 >= par.length())) {
				return null;
			}
			UiDataSto.setUnicodeBlockOffset( par );
			op = (1 <= pop) ? CLR2 : CLR1;
			break;
		case VWCAT:
			if ((null == par) || (0 >= par.length())) {
				return null;
			}
			if ('.' < par.charAt( 0 )) {
				int flags = 0;
				try {
					flags = (int) Mapping.Cats.valueOf( par ).flag;
				} catch (Exception e) {
				}
				if (0 == flags) {
					par = StringFunc.toUpperCase( par );
					try {
						flags = (int) Mapping.Cats.valueOf( par ).flag;
					} catch (Exception e) {
					}
				}
				UiDataSto.qSwitches[ UiDataSto.SWI_MAPPING_CATS ] = flags;
				UiDataSto.qSwitches[ UiDataSto.SWI_VIEW ] = UiDataSto.SW_VAL_VIEW_List;
				setToolBar( qBarTool, UiDataSto.SW_NAMES_VIEW[ qSwitches[ SWI_VIEW ] ], 0 );
			}
			op = (1 <= pop) ? CLR2 : CLR1;
			break;
		default:
			op = null;
	}
	if (op != null) {
		xyCmd.operator = op;
		xyCmd.uiParameter = null;
	}
	return xyCmd;
}

private CommandBaton handleUiEventKey( char key ) {

	CommandBaton commandData = new CommandBaton();
	commandData.operator = null;
	commandData.uiKeyOrButton = key;
	switch (key) {
		case ALT:
		case SHIFT:
			if ((UiDataSto.qPadKeys.length - 1) <= qSwitches[ UiDataSto.SWI_KEYPAD_INDEX ]) {
				if ((UiDataSto.keys_UniBlock_Current <= UiDataSto.keys_UniBlock_Offset) && (ALT == key)) {
					qSwitches[ UiDataSto.SWI_KEYPAD_INDEX ] = UiDataSto.keys_UniBlock_FromPad;
					break;
				}
				UiDataSto.setUnicodeBlock( -1, (ALT == key) ? -1 : 1 );
				break;
			}
			switchBackOrCircle( 0, SWI_KEYPAD_INDEX, (key != ALT) );
			break;
		case SEND:
			commandData.operator = QCalc.getOperator( qBarDialog[ BARI_TEXTFIELD ] );
			if (null != commandData.operator) {
				qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] = 0;
				qBarDialog[ BARI_TEXTFIELD ] = "";
				break;
			}
			// Fall through.
		case CR:
		case DONE:
		case XCOPY:
		case XCUT:
			commandData.operator = QCalc.NOP; //getOperator( "" + ((DONE != key) ? CR : key) );
			String txt = qBarDialog[ BARI_TEXTFIELD ];
			if (null != txt) {
				// No list processing yet
				txt = StringFunc.makePrintable( txt );
			}
			if ((null != txt) && (1 < txt.length())
				&& (('@' == txt.charAt( 0 )) || (':' == txt.charAt( 0 )))) {
				commandData.operator = ('@' == txt.charAt( 0 )) ? LOAD : STORE;
				txt = txt.substring( 1 );
			} else if (null != txt) {
				txt = StringFunc.string4Mnemonics( txt );
			}
			commandData.uiParameter = txt;
			final boolean fill = 0 >= txt.length();
			if (fill && (DONE != key)) {
				QVal[] arg = CcmSto.instance.stackPeek( 0 );
				if (null != arg) {
					qBarDialog[ BARI_TEXTFIELD ] = QMapSto.string4QVals( arg );
					if (XCUT == key) {
						commandData.operator = QCalc.CLEAR;
					}
				}
			} else if (XCOPY != key) {
				qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] = 0;
				qBarDialog[ BARI_TEXTFIELD ] = "";
			}
			if ((XCUT == key) || (XCOPY == key)) {
				Dib2Config.ui.pushClipboard( "edit",
					fill ? qBarDialog[ BARI_TEXTFIELD ] : commandData.uiParameter );
				commandData.uiParameter = null;
			} else {
//				if (commandData.uiParameter.startsWith( "[" )) {
//					return null;
				if ((SEND == key) && fill) {
					Dib2Config.ui.pushClipboard( "edit", qBarDialog[ BARI_TEXTFIELD ] );
					qBarDialog[ BARI_TEXTFIELD ] = "";
					commandData.operator = QCalc.DUP;
				}
			}
			break;
		case PSHIFT:
			char sel = (0 >= qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ]) ? ' '
				: qBarDialog[ BARI_TEXTFIELD ].charAt( qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] - 1 );
//			if (0 < qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ]) {
			UiDataSto.setUnicodeSelection( sel );
//				qBarDialog[ BARI_TEXTFIELD ] = qBarDialog[ BARI_TEXTFIELD ].substring( 0, qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] - 1 )
//					+ Rfc1345.next4Rfc( qBarDialog[ BARI_TEXTFIELD ].charAt( qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] - 1 ) )
//					+ qBarDialog[ BARI_TEXTFIELD ].substring( qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] );
			// Fall through
		case BACKSP:
			if (0 < qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ]) {
				-- qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ];
				qBarDialog[ BARI_TEXTFIELD ] = qBarDialog[ BARI_TEXTFIELD ].substring( 0, qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] )
					+ qBarDialog[ BARI_TEXTFIELD ].substring( qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] + 1 );
			}
			break;
		case ESCAPE:
			UiDataSto.qWinOffsetXPixR = -WIN_X_MARGIN;
			UiDataSto.qPYLineOffs = 0;
			qShiftScale = qShiftView; //PIXR_SHIFT_DEFAULT;
			break;
		case MOVE_LEFT:
			qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] -= (0 < qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ]) ? 1 : 0;
			if (0 != (UiDataSto.SW_LAYER_CANVAS & qSwitches[ SWI_LAYERS ])) {
				qPageLinesFeed.textLineCharInx -= (0 < qPageLinesFeed.textLineCharInx) ? 1 : 0;
				Dib2Config.ui.setTouched4TextPos();
			}
			break;
		case MOVE_RIGHT:
			qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] += (qSwitches[ SWI_BAR_DIALOG_CHAR_INDEX ] < qBarDialog[ BARI_TEXTFIELD ].length()) ? 1
				: 0;
			if (0 != (UiDataSto.SW_LAYER_CANVAS & qSwitches[ SWI_LAYERS ])) {
				if (0 <= qPageLinesFeed.textLineCharInx) {
					if (qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ].length() <= qPageLinesFeed.textLineCharInx) {
						qPageLinesFeed.lines[ qPageLinesFeed.textLineInx ] += ' ';
					}
					++ qPageLinesFeed.textLineCharInx;
					Dib2Config.ui.setTouched4TextPos();
				}
			}
			break;
		case SCROLL_DOWN:
			if (10 < qPageLinesFeed.lines.length) {
				UiDataSto.qPYLineOffs = (UiDataSto.qPYLineOffs >= (qPageLinesFeed.lines.length - 12 - 5)) ? (qPageLinesFeed.lines.length - 5)
					: (UiDataSto.qPYLineOffs + 12);
			}
			break;
		case SCROLL_LEFT:
			UiDataSto.qWinOffsetXPixR = (0 >= UiDataSto.qWinOffsetXPixR) ? -UiDataSto.qWinXPixR / 2
				: (UiDataSto.qWinOffsetXPixR - UiDataSto.qWinXPixR / 2);
			if ((UiDataSto.qWinOffsetXPixR > -2 * WIN_X_MARGIN) && (UiDataSto.qWinOffsetXPixR < WIN_X_MARGIN)) {
				UiDataSto.qWinOffsetXPixR = -WIN_X_MARGIN;
			}
			break;
		case SCROLL_RIGHT:
			UiDataSto.qWinOffsetXPixR += UiDataSto.qWinXPixR / 2;
			break;
		case SCROLL_UP:
			UiDataSto.qPYLineOffs = (UiDataSto.qPYLineOffs >= 12) ? (UiDataSto.qPYLineOffs - 12) : 0;
			break;
		case ZOOM_IN:
			if (0 < qShiftScale) {
				qShiftScale -= (1 + qShiftScale / PIXL_SHIFT);
				UiDataSto.qWinOffsetXPixR *= (WIN_X_MARGIN < UiDataSto.qWinOffsetXPixR) ? 2 : 1;
			}
			break;
		case ZOOM_OUT:
			if (PIXL_SHIFT > qShiftScale) {
				++ qShiftScale;
				UiDataSto.qWinOffsetXPixR /= (WIN_X_MARGIN < UiDataSto.qWinOffsetXPixR) ? 2 : 1;
			}
			break;
		default:
			commandData.uiKeyOrButton = 0;
			;
	}
	return commandData;
}

//TODO Use QBaton for events ...
@Override
public char touch( int xXReal, int xYReal, int xXSubOffset, int xYSubOffset, long xbClickclickLong ) {
	CommandBaton cmdBaton = null;
	char key = 0;
//	boolean doDraw = true;
	String sCommand = "" + CR;

	if (xYReal >= (UiDataSto.qWinYPixR - UiDataSto.qWinEYDialogBarPixR)) {
		if ( // (xYReal > (qWinYPixR - qWinEYDialogBarPixR / 2)) && 
		(xXReal > UiDataSto.qWinXPixR / 2)) {
			key = SEND;
		} else {
			handleEventDialogText( 0, 0 );
//			qDoDraw = true;
			return 0;
		}
	} else if (xYReal <= UiDataSto.qWinY1TopBarsPixR) {
		// Roughly, should match for WIN_X_MARGIN:
		int ix = (xXReal * 3 * BAR_MAX_PER_SIDE) / UiDataSto.qWinXPixR;
		key = (char) 0;
		sCommand = "";
		if (xYReal <= UiDataSto.qWinY1TopBarsPixR / 3) {
			String sKey;
			if ((ix < BAR_MAX_PER_SIDE) || (ix >= 2 * BAR_MAX_PER_SIDE)) {
				sKey = qBarTitle[ 2 * ix + 1 ];
			} else {
				return 0;
			}
			key = (0 < sKey.length()) ? sKey.charAt( 0 ) : (char) 0;
		} else if (xYReal <= 2 * UiDataSto.qWinY1TopBarsPixR / 3) {
			ix = 2 * ix + 1;
			if (1 >= ix) {
				sCommand = UiDataSto.SW_NAME_LANGUAGE__LANG;
			} else if (ix < qBarTool.length) {
				sCommand = qBarTool[ ix ];
				if (sCommand.startsWith( "LY" )) {
					sCommand = (5 >= ix) ? "LY1" : "LY2";
				}
				key = DONE;
			} else {
				return 0;
			}
		} else if (2 * BAR_MAX_PER_SIDE <= ix) {
			key = SEND;
		} else {
			return 0;
		}
		cmdBaton = evaluateEventTools( sCommand, key, xbClickclickLong );
		if ((null == cmdBaton) || (null == cmdBaton.operator)) {
			cmdBaton = handleUiEventKey( key );
			if (null == cmdBaton) {
				return 0;
			}
		}
		key = 0;
		if (null != cmdBaton.operator) {
			String op = cmdBaton.operator.getOperatorOrName();
			if (null != op) {
				key = op.charAt( 0 );
			}
		}
	} else if ((UiDataSto.SW_LAYER_CANVAS == (0xf & qSwitches[ UiDataSto.SWI_LAYERS ])) //(0 == (SW_FLAG_KEYPAD_Active & qSwitches[ SWI_LAYERS ]))
		|| (2 == xbClickclickLong)) {
		handleEventCanvas( xXReal + xXSubOffset - 0, xYReal + xYSubOffset - UiDataSto.qWinY1TopBarsPixR );
		return 0;
	} else {
		///// 7x7 fields with 1/8 padding.
		int row = (xYReal - UiDataSto.qWinY1TopBarsPixR) * qcKeys4Win * 8
			/ (UiDataSto.qWinYPixR - UiDataSto.qWinY1TopBarsPixR - UiDataSto.qWinEYDialogBarPixR);
		// Double margin for middle position, roughly:
		int col = (xXReal + WIN_X_MARGIN) * qcKeys4Win * 8 / UiDataSto.qWinXPixR;
		int rem = row % 8;
		if (((0 == rem) || (qcKeys4Win == rem)) && (8 <= row) && (48 >= row)) {
			return 0;
		}
		rem = col % 8;
		if (((0 == rem) || (qcKeys4Win == rem)) && (8 <= col) && (48 >= col)) {
			return 0;
		}
		row /= 8;
		col /= 8;
		key = qPadKeys[ qSwitches[ SWI_KEYPAD_INDEX ] ][ row * qcKeys4Win + col ];
	}
	if (null == cmdBaton) {
		cmdBaton = evaluateEventKey( key, xbClickclickLong );
		if (null == cmdBaton) {
			cmdBaton = handleUiEventKey( key );
		}
	}
//	if ((null != cmdBaton) && (null != cmdBaton.uiParameter) && (0 < cmdBaton.uiParameter.length()) && (0 < cmdBaton.uiKeyOrButton)) {
//		final QResult pooled = QResult.get8Pool();
//		CsvDb.instance.stackPush( QMap.qvalAtoms4Literal( pooled, cmdBaton.uiParameter ) );
//		cmdBaton.uiParameter = "";
//	}
	if ((null == cmdBaton) || (null == cmdBaton.operator)) {
//		doDraw = false;
		return 0;
	}
	if ((QCalc.NOP != cmdBaton.operator) || (null != cmdBaton.uiParameter)) {
		cmdBaton = handleUiEvent( cmdBaton );
		if (null != cmdBaton) { // && (NOP != cmdBaton.operator)) {
			rGateOut.push( cmdBaton );
		}
	}
	// Provide feedback: text field/ other thread showing as busy:
	Dib2Config.platform.invalidate();
	// Possibly '0' for some platforms, depending on 'restart':
	return key;
}

@Override
public boolean start() {
	QResult.getThreadIndex();
	CalcPres.wxExitRequest = false;
	MiscFunc.timeZoneDone = false;
	// TODO
	// qPadKeys = ...QWERTZ/ABC
	int minute = UiDataSto.qSwitches[ UiDataSto.SWI_STEP_OR_ACCESS_MINUTE ];
	if (1100 < minute) {
		if ((minute * 60L * 1000L + Dib2Constants.MAX_DELTA_ACCESS_CHECK) < MiscFunc.currentTimeMillisLinearized()) {
//			byte[] pass = CsvCodec0.getPassFull();
//			UiSwitches.pathInitDataFile = CcmRunner.check4Load();
//			if (null != UiSwitches.pathInitDataFile) {
			if (TcvCodec.instance.setHexPhrase( "" )) { //((null != pass) && (4 < pass.length)) {
				UiDataSto.qSwitches[ UiDataSto.SWI_STEP_OR_ACCESS_MINUTE ] = 110;
			}
		}
//	} else {
//		qAboutVw.qCcmInitialVw.setStep( qSwitches[ SWI_ACCESS_MIU_OR_INTRO ] );
	}
	return true;
}

@Override
public boolean stop( boolean fast ) {
	boolean out = true;
	if (1000 <= UiDataSto.qSwitches[ UiDataSto.SWI_STEP_OR_ACCESS_MINUTE ]) {
		wxExitRequest = true;
		if (null == TcvCodec.instance.getPassFull()) {
			TcvCodec.instance.setDummyPhrase( true );
		}
		if (fast) {
			out = false;
			CcmRunner.wxSave = true;
		} else {
//			wxExitRequest = true;
			while (true) {
				Thread rx = CcmRunner.qActive;
				if (null == rx) {
					break;
				}
				rx.interrupt();
			}
			CcmRunner.saveAll( 0 );
		}
	}
//	kPlatformUi = null;
	QResult.drop8Pool();
	return out;
}

public QBaton step() {
	return (null != wxGateIn.peek()) ? null : rGateOut.peek();
}

//=====
}
