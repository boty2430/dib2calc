// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_ui;

import io.github.gxworks.joined.Rfc1345;

import net.sf.dibdib.config.Dib2Local;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;

import static net.sf.dibdib.thread_any.StringFunc.*;

/** Constants and helpers: to be written on UI thread,
 * read-only for other threads (not enforced). */
public final class UiDataSto {
//=====

///// To be initialized

// TODO Synchronize data with preferences:
public static int[] qSwitches;

///// Startup data

public static String pathInitDataFile = null;

///// Constants

public static final int PIXL_SHIFT = 16;
public static final int PIXL_DPI = 1 << PIXL_SHIFT;
/** >160 DPI as old default for Android ~ 256 ~ (2 << 8) */
public static final int PIXR_SHIFT_DEFAULT = (PIXL_SHIFT - 8); //7 - (160+40-40)
public static final int PIXL_DPI_ADJUSTED_ANDROID = PIXL_DPI * 160 / 256;
public static final int WIN_FONT_SIZE_ANDROID = 8 * PIXL_DPI_ADJUSTED_ANDROID / 72; // 10
public static final int WIN_LINE_SPACING_ANDROID = 10 * PIXL_DPI_ADJUSTED_ANDROID / 72; // 12

public static final int WIN_BOTTOM_PIXL = 4 * WIN_LINE_SPACING_ANDROID / 3;
public static final int WIN_TOP_PIXL = 3 * 3 * WIN_LINE_SPACING_ANDROID / 2;
public static final int WIN_X_MARGIN = 8;
public static final int BAR_MAX_PER_SIDE = 4; //5
public static final int MIN_TEXT_LINES = 5; // 20;

public static final int TOUCH_TOLERANCE = 64;

public static final int SW_DEF_KEYPAD_INDEX = 1; // kKeys_1_
public static final int SW_LAYER_KEYPAD = 0x1;
public static final int SW_LAYER_CANVAS = 0x2;
public static final int SW_LAYER_UPDATE = 0x3;
public static final int SW_LAYER_KEYPAD_X = 0xf;
public static final int SW_DEF_LAYERS = 0x321; // SW_FLAG_KEYPAD_Opaque | SW_FLAG_KEYPAD_Active;
public static final int SW_VAL_LAYERS_TOUCH = 0x32;
public static final int SW_VAL_LAYERS_FEED = 0x13;
public static final int SW_VAL_KEYPAD_VAR_Dvorak = 0x1;
public static final int SW_VAL_VIEW_Intro = 0;
public static final int SW_VAL_VIEW_License = 1;
public static final int SW_VAL_VIEW_Help = 2;
public static final int SW_VAL_VIEW_Standard = 3;
public static final int SW_VAL_VIEW_List = 4;
public static final int SW_VAL_VIEWs = 5;
//RFU
public static final int SW_VAL_VIEW_Calendar = 5;

public static final String SW_NAME_LANGUAGE__LA = "LA";
public static final String SW_NAME_LANGUAGE__LANG = "LANG";
public static final String[] SW_NAMES_VIEW = { "VW0", "VWLC", "VWHL", "VWMA", "VWLS", "VWCD" };
public static final String SW_NAME_KEYPAD_DVORAK__DV = "DV";

public static final int SWI_LANGUAGE = 0;
public static final int SWI_VIEW = 1;
public static final int SWI_BAR_DIALOG_CHAR_INDEX = 2;
public static final int SWI_KEYPAD_INDEX = 3;
public static final int SWI_KEYPAD_VARIANT = 4;
public static final int SWI_LAYERS = 5;

/** Minute value (msec/1000/60) for checking access (via access code), or initial step
 * when starting the app: 0..99: init, 500..999: intro*10,
 * 100..: check AC, 200..: missing AC, 300..: missing PW. */
public static final int SWI_STEP_OR_ACCESS_MINUTE = 6;

public static final int SWI_MAPPING_CATS = 7;

public static final int BARI_TEXTFIELD_COLOR = 0;
public static final int BARI_TEXTFIELD = 1;

public static final int[] SW_DEFAULT = {
	0, SW_VAL_VIEW_Standard, 0, SW_DEF_KEYPAD_INDEX, 0, SW_DEF_LAYERS, 0, 0,
};

public static final int[] SW_INIT = {
	0, SW_VAL_VIEW_Intro, 0, SW_DEF_KEYPAD_INDEX, 0, SW_DEF_LAYERS, 0, 0,
};

///// UI data

public static CcmTemplates qTemplates = CcmTemplates.Dib2UiP_Templates_Default();

/** Latest values, continuously changing according to progress. */
public static CcmTextLines qPageLinesFeed = new CcmTextLines();

/** Stable values, used for cursor etc. */
public static CcmTextLines qPageLinesStable = new CcmTextLines();

public static int qShiftScale = PIXR_SHIFT_DEFAULT;
public static int qShiftView = PIXR_SHIFT_DEFAULT;

/** Lines instead of real pixels */
public static int qPYLineOffs;

public static int qWinWidthPixL;
public static int qWinHeightPixL;

///// UI sizes, in real pixels

public static int qWinXPixR;
public static int qWinYPixR;
public static int qPixRDpiX;
public static int qPixRDpiY;
public static int qWinY1TopBarsPixR;
public static int qWinEYDialogBarPixR;
/** Last three, real pixels */
public static int[] qWinTouchedX3PixR = new int[ 3 ];
/** Last three, real pixels */
public static int[] qWinTouchedY3PixR = new int[ 3 ];
public static int qWinOffsetXPixR = -WIN_X_MARGIN;

/////

public static final char[] kControlAsButton = {
	// 0, SEND, UP, DOWN, RIGHT, LEFT
	0, '>', '\u2191', '\u2193', '\u2192', '\u2190',
	// 0, 0, BACKSP, TAB
	0, 0, '\u226e', '\u21a0',
	// LF, VT, FF, CR
	'\u21b4', '\u21e9', 0, '\u21b2',
	// SCR-UP, SCR-DOWN, SCR-R, SCR-L, SHIFT
	'\u21d1', '\u21d3', '\u21d2', '\u21d0', '\u21e7',
	// PSHIFT, 0, 0, NBH, PASTE, COPY, CUT
	'\u21d6', 0, 0, '-', '\u21ca', '\u21c9', '\u21c8',
	// EOF, ESC, NBS
	'\u2584', '\u2297', ' ',
	// ZOOM-IN, ZOOM-OUT, SH
	'\u2295', '\u2299', '\u2194',
};

public static final char[] kKeys_0 = {
	'ä', 'ê', MOVE_UP, SCROLL_UP, 'ë', 'ü', BACKSP,
	'f', 'g', 'ç', 'r', 'l', '/', ' ',
	MOVE_LEFT, 'à', 'ô', 'é', 'û', 'î', ' ',
	SCROLL_LEFT, 'd', 'h', 't', 'n', 'ß', SCROLL_RIGHT,
	TAB, 'â', 'ö', 'è', 'ù', 'ï', MOVE_RIGHT,
	SHIFT, 'b', 'm', 'w', 'v', 'Z', PSHIFT,
	ALT, ',', ' ', SCROLL_DOWN, MOVE_DOWN, '.', CR,
};

public static final char[] kKeys_1 = {
	'_', '{', MOVE_UP, SCROLL_UP, '&', '}', BACKSP,
	'/', '[', '%', '0', '=', ']', '!',
	MOVE_LEFT, '(', '1', '2', '3', ')', '-',
	SCROLL_LEFT, '<', '4', '5', '6', '>', SCROLL_RIGHT,
	TAB, XCUT, '7', '8', '9', '0', MOVE_RIGHT,
	SHIFT, XPASTE, '*', '0', '#', '+', PSHIFT,
	ALT, XCOPY, ' ', SCROLL_DOWN, MOVE_DOWN, '.', CR,
};

public static final char[] kKeys_1_calc = {
	'_', '{', '\u2228', SCROLL_UP, '&', '}', BACKSP,
	'/', '[', '%', /*'\u22bb'*/'\u2207', '!', ']', '\u00b1',
	/*'\u215f'*/'\u00b9', '(', '1', '2', '3', ')', '-',
	SCROLL_LEFT, '\u00ab', '4', '5', '6', '\u00bb', SCROLL_RIGHT,
	'A', 'B', '7', '8', '9', 'E', 'F',
	SHIFT, 'C', '*', '0', '#', '+', PSHIFT,
	ALT, 'D', ' ', SCROLL_DOWN, '\u2206', '.', CR,
};

public static final char[] kKeys_a_Dvorak = {
	'_', '\'', MOVE_UP, SCROLL_UP, 'p', 'y', BACKSP,
	'/', 'f', 'g', 'c', 'r', 'l', '?',
	MOVE_LEFT, 'a', 'o', 'e', 'u', 'i', '-',
	SCROLL_LEFT, 'd', 'h', 't', 'n', 's', SCROLL_RIGHT,
	TAB, '!', 'q', 'j', 'k', 'x', MOVE_RIGHT,
	SHIFT, 'b', 'm', 'w', 'v', 'z', PSHIFT,
	ALT, ',', ' ', SCROLL_DOWN, MOVE_DOWN, '.', CR,
};

public static final char[] kKeys_a = kKeys_a_Dvorak.clone();

public static final char[] kKeys_A_Dvorak = {
	'_', '"', MOVE_UP, SCROLL_UP, 'P', 'Y', BACKSP,
	'/', 'F', 'G', 'C', 'R', 'L', '!',
	MOVE_LEFT, 'A', 'O', 'E', 'U', 'I', '-',
	SCROLL_LEFT, 'D', 'H', 'T', 'N', 'S', SCROLL_RIGHT,
	TAB, ':', 'Q', 'J', 'K', 'X', MOVE_RIGHT,
	SHIFT, 'B', 'M', 'W', 'V', 'Z', PSHIFT,
	ALT, ',', ' ', SCROLL_DOWN, MOVE_DOWN, '.', CR,
};

public static final char[] kKeys_A = kKeys_A_Dvorak.clone();
public static final char[] keys_UnicodeSel = kKeys_A_Dvorak.clone();
public static int keys_UniBlock_Offset = 0x2200;
public static int keys_UniBlock_Current = keys_UniBlock_Offset;
public static int keys_UniBlock_FromPad = 1;

public static final char[] kKeys_Z = {
	'Ä', 'Ê', MOVE_UP, SCROLL_UP, 'Ë', 'Ü', BACKSP,
	'F', 'G', 'Ç', 'R', 'L', '/', ' ',
	MOVE_LEFT, 'À', 'Ô', 'É', 'Û', 'Î', ' ',
	SCROLL_LEFT, 'D', 'H', 'T', 'N', 'ß', SCROLL_RIGHT,
	TAB, 'Â', 'Ö', 'È', 'Ù', 'Ï', MOVE_RIGHT,
	SHIFT, 'B', 'M', 'W', 'V', 'Z', PSHIFT,
	ALT, ',', ' ', SCROLL_DOWN, MOVE_DOWN, '.', CR,
};

public static final char[][] kPadKeys_Dvorak = { kKeys_0, kKeys_1, kKeys_a_Dvorak, kKeys_A_Dvorak, kKeys_Z };
public static final char[][] kPadKeys_Calc = { kKeys_0, kKeys_1_calc, kKeys_a, kKeys_A, keys_UnicodeSel };

public static final String[] kBarTitleDefault = {
	// LEFT
	"RED", "" + ESCAPE, "BLUE", "" + ZOOM_OUT, "BLUE", "" + ZOOM_IN, //"", " ", 
	"BLUE", "A",
	// CENTER
	"", " ", "", "Dib2x", "", "", "", "", //"", "",
	// RIGHT
	"RED", "" + XCUT, "GREEN", "" + XPASTE, "BLUE", "" + XCOPY, //"", " ", 
	"", "=",
};

public static final String[] kBarToolDefault = {
	"", SW_NAME_LANGUAGE__LANG, "", SW_NAMES_VIEW[ SW_VAL_VIEW_Standard ], "", "LYKC", "", "LYCU",
	"", "QFIL", "", "  GO", "", " CLR", "", "STOR", "", "LOAD", "", " ADD", "", "SUB ", "", "MUL ", "", "DIV ",
};

public static final String[] kBarDialogDefault = {
	// Text field
	"", "",
	// SEND
	"", " ", "", " ", "", " ", "GREEN", ">",
};

public static final String[] kBarStatus = {
	"GREEN", ">", "", " ", "", " ", "", "OK",
};

/////

public static String[] qBarTitle = kBarTitleDefault.clone();
public static String[] qBarTool = kBarToolDefault.clone();
public static String[] qBarDialog = kBarDialogDefault.clone();
public static String[] qBarStatus = kBarStatus.clone();
public static char[][] qPadKeys = kPadKeys_Calc;
public static int qcKeys4Win = 7;

private static int[] zSwitchBack = SW_DEFAULT.clone();

static {
	char cA = 'Z';
	char ca = 'z';
	for (int i0 = kKeys_a.length - 1; (i0 >= 0) && ('A' <= cA); -- i0) {
		kKeys_A[ i0 ] = ('A' <= kKeys_A[ i0 ]) ? (cA --) : kKeys_A[ i0 ];
		kKeys_a[ i0 ] = ('A' <= kKeys_a[ i0 ]) ? (ca --) : kKeys_a[ i0 ];
	}
	setUnicodeBlock( keys_UniBlock_Current, 0 );
}

private static int setUnicodeBlock_perRound = 0;

public static int setUnicodeBlock( int block, int rounds ) {
	block = (0 > block) ? keys_UniBlock_Current : block;
	if (0 >= setUnicodeBlock_perRound) {
		int tot = 0;
		for (int i0 = 0; i0 < keys_UnicodeSel.length; ++ i0) {
			tot += (kKeys_A_Dvorak[ i0 ] > '.') ? 1 : 0;
		}
		setUnicodeBlock_perRound = tot;
	}
	keys_UniBlock_Current = (block + rounds * setUnicodeBlock_perRound) & 0xffff;
	if (0xd800 <= keys_UniBlock_Current) {
		keys_UniBlock_Current = 0x2000;
	}
	char c0 = (char) (keys_UniBlock_Current + setUnicodeBlock_perRound);
	for (int i0 = keys_UnicodeSel.length - 1; i0 >= 0; -- i0) {
		keys_UnicodeSel[ i0 ] = (kKeys_A_Dvorak[ i0 ] > '.') ? (-- c0) : kKeys_A_Dvorak[ i0 ];
	}
	keys_UniBlock_FromPad = 1;
	return keys_UniBlock_Current;
}

public static void setUnicodeBlockOffset( String blockNameOrOffset ) {
	int block = (int) StringFunc.long4String( blockNameOrOffset, -1 );
	if (0 > block) {
		for (int i0 = 1; i0 < Rfc1345.kUnicodeBlocks.length; i0 += 2) {
			if (Rfc1345.kUnicodeBlocks[ i0 ].contains( blockNameOrOffset )) {
				block = Rfc1345.kUnicodeBlocks[ i0 - 1 ].charAt( 0 );
			}
		}
	}
	if (0 <= block) {
		keys_UniBlock_Offset = setUnicodeBlock( block, 0 );
	}
}

public static void setUnicodeSelection( char base ) {
	keys_UniBlock_Offset = setUnicodeBlock( base, 0 );
	keys_UniBlock_FromPad = qSwitches[ UiDataSto.SWI_KEYPAD_INDEX ];
	String group = StringFunc.group4Rfc1345( base );
	int count = 0;
	final int len = (group.length() > keys_UnicodeSel.length) ? keys_UnicodeSel.length : group.length();
	for (int i0 = 0; i0 < len; ++ i0) {
		if (kKeys_A_Dvorak[ i0 ] <= ' ') {
			keys_UnicodeSel[ i0 ] = kKeys_A_Dvorak[ i0 ];
		} else {
			keys_UnicodeSel[ i0 ] = group.charAt( count ++ );
		}
	}
	qSwitches[ UiDataSto.SWI_KEYPAD_INDEX ] = qPadKeys.length - 1;
}

public static void switchBackOrCircle( long xbClickclickLong, int xiSwitch, boolean xUp ) {
	int nextValue = qSwitches[ xiSwitch ] + (xUp ? 1 : -1);
	int switchMin = 0;
	int switchCeil = 2;
	switch (xiSwitch) {
		case SWI_KEYPAD_INDEX:
			switchCeil = qPadKeys.length;
			break;
		case SWI_LANGUAGE:
			switchCeil = Dib2Local.kLanguages.length;
			break;
		case SWI_LAYERS:
//			switchLayers( xUp );
			qSwitches[ SWI_LAYERS ] = (SW_LAYER_KEYPAD == (qSwitches[ SWI_LAYERS ] & 0xf)) ? SW_VAL_LAYERS_TOUCH
				: ((SW_LAYER_CANVAS == (qSwitches[ SWI_LAYERS ] & 0xf)) ? SW_VAL_LAYERS_FEED : SW_DEF_LAYERS);
			setBar4Layers();
			return;
			// KEYPAD:
//			nextValue = (1 + (0x3 & xySwitches[ SWI_LAYERS ])) & 0x3;
//			nextValue = (0x2 == nextValue) ? 3 : nextValue;
//			nextValue = (nextValue & ~0xff) | ((nextValue >>> 4) & 0xf) | ((nextValue & 0xf) << 4);
//			switchCeil = 0xffff;
		case SWI_VIEW:
			qSwitches[ SWI_VIEW ] = (0 != (2 & xbClickclickLong))
				? SW_DEFAULT[ SWI_VIEW ]
				: (((nextValue - 1) % 4) + 1); // %3;
			//Dib2UiP.INSTANCE.wqRefreshView;
			return;
		default:
			;
	}
	if ((0 == (2 & xbClickclickLong)) || (zSwitchBack[ xiSwitch ] == SW_DEFAULT[ xiSwitch ])) {
		// Circle values.
		qSwitches[ xiSwitch ] = nextValue;
		if (switchMin > qSwitches[ xiSwitch ]) {
			qSwitches[ xiSwitch ] = SW_DEFAULT[ xiSwitch ];
		}
		if (switchCeil <= qSwitches[ xiSwitch ]) {
			qSwitches[ xiSwitch ] = SW_DEFAULT[ xiSwitch ];
		}
		zSwitchBack[ xiSwitch ] = qSwitches[ xiSwitch ];
	} else {
		// Switch values.
		qSwitches[ xiSwitch ] = (SW_DEFAULT[ xiSwitch ] == qSwitches[ xiSwitch ]) ? zSwitchBack[ xiSwitch ] : SW_DEFAULT[ xiSwitch ];
	}
}

public static void setSwitchBackValue( int[] xySwitches, int xiSwitch ) {
	zSwitchBack[ xiSwitch ] = xySwitches[ xiSwitch ];
}

public static void setToolBar( String[] xyBarTool, String xTool, int xValue ) {
	if (xTool.startsWith( "LA" )) {
		xyBarTool[ 1 ] = xTool;
	} else if (xTool.startsWith( "LY" )) {
		final String markers = ".KCU..........X";
		final char mid = markers.charAt( (xValue >> 4) & 0xf );
		xyBarTool[ 5 ] = "LY" + markers.charAt( xValue & 0xf ) + mid;
		xyBarTool[ 7 ] = "LY" + mid + markers.charAt( (xValue >> 8) & 0xf );
	} else if (xTool.startsWith( "VW" )) {
		xyBarTool[ 3 ] = xTool;
	}
}

public static void switchView8Init( int[] xySwitches, String[] xyBarTitle, String[] xyBarTool ) {
	xySwitches[ UiDataSto.SWI_STEP_OR_ACCESS_MINUTE ] = (int) (MiscFunc.currentTimeMillisLinearized() / 1000 / 60);
	xyBarTool[ 1 ] = "LA" + Dib2Local.kLanguages[ xySwitches[ SWI_LANGUAGE ] ];
	xySwitches[ UiDataSto.SWI_VIEW ] = UiDataSto.SW_VAL_VIEW_Standard;
	xyBarTool[ 3 ] = UiDataSto.SW_NAMES_VIEW[ UiDataSto.SW_VAL_VIEW_Standard ];
}

public static void setBar4Layers() {
	if (null != qBarTitle) {
		qBarTitle[ 2 * 3 + 1 ] = (UiDataSto.SW_LAYER_CANVAS == (0xf & qSwitches[ UiDataSto.SWI_LAYERS ])) ? "X" : "A";
		qBarTitle[ 2 * 3 ] = (UiDataSto.SW_VAL_LAYERS_FEED == qSwitches[ UiDataSto.SWI_LAYERS ]) ? "GREEN" : "BLUE"; // "";
		setToolBar( qBarTool, "LY", qSwitches[ SWI_LAYERS ] );
	}
}

public static void switchLayers( boolean higher ) {
	int v1 = qSwitches[ SWI_LAYERS ] & 0xf;
	int v2 = (qSwitches[ SWI_LAYERS ] >>> 4) & 0xf;
	int v3 = (qSwitches[ SWI_LAYERS ] >>> 8) & 0xf;
	if (higher) {
		if (SW_LAYER_KEYPAD == v1) {
			++ v2;
			v2 += (v1 == v2) ? 1 : 0;
			if (3 < v2) {
				v2 = 0;
				++ v1;
			}
		} else if (0 == v2) {
			v2 = SW_LAYER_KEYPAD;
		} else {
			++ v1;
			v2 = 0;
		}
		if (3 < v1) {
			v1 = 1;
			v2 = 0;
		}
	} else {
		if (0 == v2) {
			v2 = (1 == v1) ? 2 : 1;
		} else {
			do {
				v3 = (v3 + 1) % 4;
				v2 = (0 == v3) ? ((v2 + 1) % 4) : v2;
				if (0 == v2) {
					break;
				}
			} while ((v1 == v2) || (v2 == v3) || (v1 == v3));
		}
	}
	setBar4Layers();
}

//=====
}
