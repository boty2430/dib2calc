// Copyright (C) 2018,2019 Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_ui;

import net.sf.dibdib.thread_any.*;

final class CalcVw extends CcmVwIf {
//=====

String[] out = new String[ 100 ];

@Override
String[] getTextLines() {
	return out;
}

@Override
int prepareTextLines( boolean is4Console, String... param ) {
	long xbHexMemory = 3;
	int xOffset = 1;
	boolean partial = false;
	out = (null == out) ? new String[ 30 ] : out;
	int len = CcmSto.instance.stackRead( xbHexMemory, out, xOffset, partial );
	// Need some 10 lines for possible memory values.
	if ((0 <= len) && !partial && ((len + 10) >= out.length)) {
		len = -len / 2 - 10 / 2 - 2;
	}
	if (0 > len) {
		out = new String[ -len * 2 + 2 ];
		len = CcmSto.instance.stackRead( xbHexMemory, out, xOffset, true );
	}
	out[ 0 ] = ((null == param) || (0 >= param.length)) ? "" : ("\t" + param[ 0 ]);
	for (int i0 = out.length - 1; i0 >= len; -- i0) {
		out[ i0 ] = null;
	}
	for (int i0 = len - 1; i0 >= 0; -- i0) {
		out[ i0 ] = StringFunc.makePrintable( out[ i0 ] );
	}
	return len;
}

//=====
}
